# Nirvati API

This is the main GraphQL API for Nirvati, which is exposed through HTTPS.
It is mostly a gateway to the core services, which are exposed through gRPC.
This API implements user management, authentication, authorization, and
brings together some low-level operations from the core services.
