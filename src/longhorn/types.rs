use std::collections::HashMap;

use serde::{Deserialize, Serialize};
use serde_aux::prelude::*;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ApiVersion {
    pub links: HashMap<String, String>,
    pub actions: HashMap<String, String>,
    pub r#type: String,
    pub id: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Schema {
    #[serde(rename = "pluralName")]
    pub plural_name: String,
    #[serde(rename = "collectionActions")]
    pub collection_actions: HashMap<String, String>,
    #[serde(rename = "resourceFields")]
    pub resource_fields: HashMap<String, String>,
    #[serde(rename = "collectionFields")]
    pub collection_fields: HashMap<String, String>,
    pub id: String,
    #[serde(rename = "collectionMethods")]
    pub collection_methods: Vec<String>,
    #[serde(rename = "includeableLinks")]
    pub includeable_links: Vec<String>,
    pub actions: HashMap<String, String>,
    pub links: HashMap<String, String>,
    #[serde(rename = "resourceMethods")]
    pub resource_methods: Vec<String>,
    #[serde(rename = "collectionFilters")]
    pub collection_filters: HashMap<String, String>,
    pub r#type: String,
    #[serde(rename = "resourceActions")]
    pub resource_actions: HashMap<String, String>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Error {
    pub actions: HashMap<String, String>,
    pub links: HashMap<String, String>,
    pub status: i32,
    pub r#type: String,
    pub code: String,
    pub message: String,
    pub detail: String,
    pub id: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AttachInput {
    #[serde(rename = "attacherType")]
    pub attacher_type: String,
    #[serde(rename = "attachedBy")]
    pub attached_by: String,
    #[serde(rename = "hostId")]
    pub host_id: String,
    #[serde(rename = "disableFrontend")]
    pub disable_frontend: bool,
    #[serde(rename = "attachmentID")]
    pub attachment_id: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DetachInput {
    #[serde(rename = "hostId")]
    pub host_id: String,
    #[serde(rename = "forceDetach")]
    pub force_detach: bool,
    #[serde(rename = "attachmentID")]
    pub attachment_id: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SnapshotInput {
    pub labels: HashMap<String, String>,
    pub name: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SnapshotCrInput {
    pub name: String,
    pub labels: HashMap<String, String>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BackupTarget {
    pub actions: HashMap<String, String>,
    #[serde(rename = "backupTargetURL")]
    pub backup_target_url: String,
    pub message: String,
    pub r#type: String,
    pub available: bool,
    pub id: String,
    pub links: HashMap<String, String>,
    #[serde(rename = "credentialSecret")]
    pub credential_secret: String,
    #[serde(rename = "pollInterval")]
    pub poll_interval: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Backup {
    pub actions: HashMap<String, String>,
    #[serde(rename = "volumeSize")]
    pub volume_size: String,
    #[serde(rename = "volumeBackingImageName")]
    pub volume_backing_image_name: String,
    pub labels: HashMap<String, String>,
    pub state: String,
    pub url: String,
    #[serde(rename = "volumeName")]
    pub volume_name: String,
    #[serde(rename = "snapshotCreated")]
    pub snapshot_created: String,
    pub name: String,
    pub links: HashMap<String, String>,
    pub size: String,
    #[serde(rename = "volumeCreated")]
    pub volume_created: String,
    #[serde(rename = "compressionMethod")]
    pub compression_method: String,
    pub error: String,
    #[serde(rename = "snapshotName")]
    pub snapshot_name: String,
    pub created: String,
    pub messages: HashMap<String, String>,
    pub progress: i32,
    pub id: String,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BackupInput {
    pub name: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BackupStatus {
    pub actions: HashMap<String, String>,
    pub size: String,
    pub r#type: String,
    pub links: HashMap<String, String>,
    pub progress: i32,
    pub replica: String,
    pub state: String,
    #[serde(rename = "backupURL")]
    pub backup_url: String,
    pub snapshot: String,
    pub error: String,
    pub id: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Orphan {
    pub actions: HashMap<String, String>,
    pub r#type: String,
    pub links: HashMap<String, String>,
    pub id: String,
    #[serde(rename = "orphanType")]
    pub orphan_type: String,
    #[serde(rename = "nodeID")]
    pub node_id: String,
    pub parameters: HashMap<String, String>,
    pub name: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RestoreStatus {
    pub id: Option<String>,
    #[serde(rename = "isRestoring")]
    pub is_restoring: bool,
    pub replica: String,
    pub progress: i32,
    pub error: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    #[serde(rename = "backupURL")]
    pub backup_url: String,
    #[serde(rename = "lastRestored")]
    pub last_restored: String,
    pub state: String,
    pub filename: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    pub r#type: Option<String>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PurgeStatus {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    pub progress: i32,
    pub error: String,
    pub replica: String,
    pub state: String,
    pub id: Option<String>,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    #[serde(rename = "isPurging")]
    pub is_purging: bool,
    pub r#type: Option<String>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RebuildStatus {
    #[serde(rename = "fromReplica")]
    pub from_replica: String,
    pub id: String,
    pub progress: i32,
    #[serde(rename = "isRebuilding")]
    pub is_rebuilding: bool,
    pub error: String,
    pub state: String,
    pub r#type: Option<String>,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    pub replica: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ReplicaRemoveInput {
    pub name: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SalvageInput {
    pub names: Vec<String>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ActivateInput {
    pub frontend: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ExpandInput {
    pub size: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct EngineUpgradeInput {
    pub image: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Replica {
    pub address: String,
    #[serde(rename = "currentImage")]
    pub current_image: String,
    #[serde(rename = "dataEngine")]
    pub data_engine: String,
    #[serde(rename = "failedAt")]
    pub failed_at: String,
    #[serde(rename = "hostId")]
    pub host_id: String,
    #[serde(rename = "instanceManagerName")]
    pub instance_manager_name: String,
    pub mode: String,
    pub name: String,
    pub running: bool,
    #[serde(rename = "diskPath")]
    pub disk_path: String,
    #[serde(rename = "dataPath")]
    pub data_path: String,
    pub image: String,
    #[serde(rename = "diskID")]
    pub disk_id: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Controller {
    #[serde(rename = "lastExpansionError")]
    pub last_expansion_error: String,
    #[serde(rename = "currentImage")]
    pub current_image: String,
    #[serde(rename = "lastRestoredBackup")]
    pub last_restored_backup: String,
    #[serde(rename = "lastExpansionFailedAt")]
    pub last_expansion_failed_at: String,
    pub size: String,
    pub endpoint: String,
    #[serde(rename = "instanceManagerName")]
    pub instance_manager_name: String,
    pub name: String,
    #[serde(rename = "hostId")]
    pub host_id: String,
    pub image: String,
    #[serde(rename = "isExpanding")]
    pub is_expanding: bool,
    pub running: bool,
    #[serde(rename = "unmapMarkSnapChainRemovedEnabled")]
    pub unmap_mark_snap_chain_removed_enabled: bool,
    #[serde(rename = "actualSize")]
    pub actual_size: String,
    #[serde(rename = "requestedBackupRestore")]
    pub requested_backup_restore: String,
    pub address: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DiskUpdate {
    pub tags: Vec<String>,
    #[serde(rename = "diskType")]
    pub disk_type: String,
    #[serde(rename = "storageReserved")]
    pub storage_reserved: i32,
    pub path: String,
    #[serde(rename = "allowScheduling")]
    pub allow_scheduling: bool,
    #[serde(rename = "evictionRequested")]
    pub eviction_requested: bool,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateReplicaCountInput {
    #[serde(rename = "replicaCount")]
    pub replica_count: i32,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateReplicaAutoBalanceInput {
    #[serde(rename = "replicaAutoBalance")]
    pub replica_auto_balance: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateDataLocalityInput {
    #[serde(rename = "dataLocality")]
    pub data_locality: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateAccessModeInput {
    #[serde(rename = "accessMode")]
    pub access_mode: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateSnapshotDataIntegrityInput {
    #[serde(rename = "snapshotDataIntegrity")]
    pub snapshot_data_integrity: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateSnapshotMaxCountInput {
    #[serde(rename = "snapshotMaxCount")]
    pub snapshot_max_count: i32,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateSnapshotMaxSizeInput {
    #[serde(rename = "snapshotMaxSize")]
    pub snapshot_max_size: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateOfflineReplicaRebuildingInput {
    #[serde(rename = "offlineReplicaRebuilding")]
    pub offline_replica_rebuilding: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateBackupCompressionInput {
    #[serde(rename = "backupCompressionMethod")]
    pub backup_compression_method: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateUnmapMarkSnapChainRemovedInput {
    #[serde(rename = "unmapMarkSnapChainRemoved")]
    pub unmap_mark_snap_chain_removed: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateReplicaSoftAntiAffinityInput {
    #[serde(rename = "replicaSoftAntiAffinity")]
    pub replica_soft_anti_affinity: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateReplicaZoneSoftAntiAffinityInput {
    #[serde(rename = "replicaZoneSoftAntiAffinity")]
    pub replica_zone_soft_anti_affinity: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateReplicaDiskSoftAntiAffinityInput {
    #[serde(rename = "replicaDiskSoftAntiAffinity")]
    pub replica_disk_soft_anti_affinity: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct WorkloadStatus {
    #[serde(rename = "podName")]
    pub pod_name: String,
    #[serde(rename = "podStatus")]
    pub pod_status: String,
    #[serde(rename = "workloadName")]
    pub workload_name: String,
    #[serde(rename = "workloadType")]
    pub workload_type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CloneStatus {
    pub snapshot: String,
    #[serde(rename = "sourceVolume")]
    pub source_volume: String,
    pub state: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Empty {
    pub actions: HashMap<String, String>,
    pub id: String,
    pub links: HashMap<String, String>,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct VolumeRecurringJob {
    pub r#type: String,
    pub name: String,
    #[serde(rename = "isGroup")]
    pub is_group: bool,
    pub id: String,
    pub actions: HashMap<String, String>,
    pub links: HashMap<String, String>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct VolumeRecurringJobInput {
    pub name: String,
    #[serde(rename = "isGroup")]
    pub is_group: bool,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PvCreateInput {
    #[serde(rename = "fsType")]
    pub fs_type: String,
    #[serde(rename = "pvName")]
    pub pv_name: String,
    #[serde(rename = "secretName")]
    pub secret_name: String,
    #[serde(rename = "secretNamespace")]
    pub secret_namespace: String,
    #[serde(rename = "storageClassName")]
    pub storage_class_name: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PvcCreateInput {
    pub namespace: String,
    #[serde(rename = "pvcName")]
    pub pvc_name: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SettingDefinition {
    pub description: String,
    pub category: String,
    pub options: Vec<String>,
    #[serde(rename = "readOnly")]
    pub read_only: bool,
    pub required: bool,
    pub default: String,
    pub r#type: String,
    #[serde(rename = "displayName")]
    pub display_name: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct VolumeCondition {
    pub status: String,
    pub message: String,
    #[serde(rename = "lastTransitionTime")]
    pub last_transition_time: String,
    pub reason: String,
    pub r#type: String,
    #[serde(rename = "lastProbeTime")]
    pub last_probe_time: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct NodeCondition {
    pub message: String,
    #[serde(rename = "lastProbeTime")]
    pub last_probe_time: String,
    pub reason: String,
    pub status: String,
    pub r#type: String,
    #[serde(rename = "lastTransitionTime")]
    pub last_transition_time: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DiskCondition {
    pub status: String,
    pub r#type: String,
    #[serde(rename = "lastProbeTime")]
    pub last_probe_time: String,
    #[serde(rename = "lastTransitionTime")]
    pub last_transition_time: String,
    pub message: String,
    pub reason: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct LonghornCondition {
    pub r#type: String,
    #[serde(rename = "lastProbeTime")]
    pub last_probe_time: String,
    #[serde(rename = "lastTransitionTime")]
    pub last_transition_time: String,
    pub reason: String,
    pub status: String,
    pub message: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Event {
    pub actions: HashMap<String, String>,
    #[serde(rename = "eventType")]
    pub event_type: String,
    pub event: k8s_openapi::api::core::v1::Event,
    pub r#type: String,
    pub id: String,
    pub links: HashMap<String, String>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SupportBundle {
    pub actions: HashMap<String, String>,
    #[serde(rename = "progressPercentage")]
    pub progress_percentage: i32,
    #[serde(rename = "nodeID")]
    pub node_id: String,
    pub r#type: String,
    pub id: String,
    pub name: String,
    #[serde(rename = "errorMessage")]
    pub error_message: String,
    pub state: String,
    pub links: HashMap<String, String>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SupportBundleInitateInput {
    pub description: String,
    #[serde(rename = "issueURL")]
    pub issue_url: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Tag {
    #[serde(rename = "tagType")]
    pub tag_type: String,
    pub actions: HashMap<String, String>,
    pub links: HashMap<String, String>,
    pub name: String,
    pub id: String,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct InstanceManager {
    #[serde(rename = "currentState")]
    pub current_state: String,
    #[serde(rename = "nodeID")]
    pub node_id: String,
    pub actions: HashMap<String, String>,
    #[serde(rename = "instanceEngines")]
    pub instance_engines: HashMap<String, String>,
    pub id: String,
    #[serde(rename = "instanceReplicas")]
    pub instance_replicas: HashMap<String, String>,
    #[serde(rename = "dataEngine")]
    pub data_engine: String,
    pub instances: HashMap<String, String>,
    pub image: String,
    pub links: HashMap<String, String>,
    #[serde(rename = "managerType")]
    pub manager_type: String,
    pub r#type: String,
    pub name: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BackingImageDiskFileStatus {
    pub message: String,
    #[serde(rename = "lastStateTransitionTime")]
    pub last_state_transition_time: String,
    pub state: String,
    pub progress: i32,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BackingImageCleanupInput {
    pub disks: Vec<String>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Attachment {
    #[serde(rename = "attachmentType")]
    pub attachment_type: String,
    pub conditions: Vec<LonghornCondition>,
    #[serde(rename = "nodeID")]
    pub node_id: String,
    pub satisfied: bool,
    #[serde(rename = "attachmentID")]
    pub attachment_id: String,
    pub parameters: HashMap<String, String>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct VolumeAttachment {
    pub attachments: HashMap<String, Attachment>,
    pub volume: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Volume {
    #[serde(rename = "currentImage")]
    pub current_image: String,
    #[serde(rename = "replicaZoneSoftAntiAffinity")]
    pub replica_zone_soft_anti_affinity: String,
    #[serde(rename = "restoreVolumeRecurringJob")]
    pub restore_volume_recurring_job: String,
    #[serde(
        rename = "purgeStatus",
        deserialize_with = "deserialize_default_from_null"
    )]
    pub purge_status: Vec<PurgeStatus>,
    pub size: String,
    #[serde(rename = "backupCompressionMethod")]
    pub backup_compression_method: String,
    pub frontend: String,
    #[serde(rename = "lastBackupAt")]
    pub last_backup_at: String,
    pub id: String,
    #[serde(rename = "lastBackup")]
    pub last_backup: String,
    pub encrypted: bool,
    pub links: HashMap<String, String>,
    pub migratable: bool,
    #[serde(rename = "rebuildStatus")]
    pub rebuild_status: Vec<RebuildStatus>,
    #[serde(rename = "shareState")]
    pub share_state: String,
    #[serde(rename = "unmapMarkSnapChainRemoved")]
    pub unmap_mark_snap_chain_removed: String,
    #[serde(rename = "snapshotMaxSize")]
    pub snapshot_max_size: String,
    #[serde(rename = "diskSelector")]
    pub disk_selector: Vec<String>,
    #[serde(rename = "offlineReplicaRebuilding")]
    pub offline_replica_rebuilding: String,
    #[serde(rename = "replicaAutoBalance")]
    pub replica_auto_balance: String,
    #[serde(rename = "dataEngine")]
    pub data_engine: String,
    #[serde(rename = "shareEndpoint")]
    pub share_endpoint: String,
    #[serde(rename = "kubernetesStatus")]
    pub kubernetes_status: KubernetesStatus,
    #[serde(rename = "offlineReplicaRebuildingRequired")]
    pub offline_replica_rebuilding_required: bool,
    #[serde(rename = "cloneStatus")]
    pub clone_status: CloneStatus,
    pub ready: bool,
    #[serde(rename = "restoreInitiated")]
    pub restore_initiated: bool,
    #[serde(rename = "numberOfReplicas")]
    pub number_of_replicas: i32,
    #[serde(rename = "lastAttachedBy")]
    pub last_attached_by: String,
    #[serde(rename = "revisionCounterDisabled")]
    pub revision_counter_disabled: bool,
    #[serde(rename = "snapshotDataIntegrity")]
    pub snapshot_data_integrity: String,
    pub conditions: HashMap<String, VolumeCondition>,
    #[serde(rename = "snapshotMaxCount")]
    pub snapshot_max_count: i32,
    #[serde(rename = "disableFrontend")]
    pub disable_frontend: bool,
    #[serde(rename = "staleReplicaTimeout")]
    pub stale_replica_timeout: i32,
    pub replicas: Vec<Replica>,
    pub r#type: String,
    pub name: String,
    pub created: String,
    #[serde(rename = "nodeSelector")]
    pub node_selector: Vec<String>,
    #[serde(
        rename = "recurringJobSelector",
        deserialize_with = "deserialize_default_from_null"
    )]
    pub recurring_job_selector: Vec<VolumeRecurringJob>,
    #[serde(rename = "restoreStatus")]
    pub restore_status: Vec<RestoreStatus>,
    pub robustness: String,
    pub controllers: Vec<Controller>,
    #[serde(rename = "fromBackup")]
    pub from_backup: String,
    #[serde(rename = "restoreRequired")]
    pub restore_required: bool,
    #[serde(rename = "replicaDiskSoftAntiAffinity")]
    pub replica_disk_soft_anti_affinity: String,
    pub actions: HashMap<String, String>,
    #[serde(rename = "accessMode")]
    pub access_mode: String,
    #[serde(rename = "replicaSoftAntiAffinity")]
    pub replica_soft_anti_affinity: String,
    #[serde(rename = "dataSource")]
    pub data_source: String,
    #[serde(rename = "dataLocality")]
    pub data_locality: String,
    pub standby: bool,
    pub state: String,
    #[serde(rename = "backupStatus")]
    pub backup_status: Vec<BackupStatus>,
    #[serde(rename = "backingImage")]
    pub backing_image: String,
    #[serde(rename = "volumeAttachment")]
    pub volume_attachment: VolumeAttachment,
    pub image: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Snapshot {
    pub labels: HashMap<String, String>,
    pub links: HashMap<String, String>,
    pub size: String,
    pub id: String,
    pub checksum: String,
    pub created: String,
    pub removed: bool,
    pub usercreated: bool,
    pub r#type: String,
    pub actions: HashMap<String, String>,
    pub children: HashMap<String, bool>,
    pub name: String,
    pub parent: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SnapshotCr {
    #[serde(rename = "crCreationTime")]
    pub cr_creation_time: String,
    #[serde(rename = "markRemoved")]
    pub mark_removed: bool,
    pub name: String,
    #[serde(rename = "creationTime")]
    pub creation_time: String,
    pub volume: String,
    #[serde(rename = "ownerID")]
    pub owner_id: String,
    pub size: i32,
    pub id: String,
    pub error: String,
    pub children: HashMap<String, bool>,
    pub links: HashMap<String, String>,
    pub parent: String,
    pub checksum: String,
    #[serde(rename = "createSnapshot")]
    pub create_snapshot: bool,
    pub actions: HashMap<String, String>,
    pub labels: HashMap<String, String>,
    #[serde(rename = "readyToUse")]
    pub ready_to_use: bool,
    pub r#type: String,
    #[serde(rename = "restoreSize")]
    pub restore_size: i32,
    #[serde(rename = "userCreated")]
    pub user_created: bool,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BackupVolume {
    #[serde(rename = "lastBackupAt")]
    pub last_backup_at: String,
    #[serde(rename = "dataStored")]
    pub data_stored: String,
    #[serde(rename = "backingImageChecksum")]
    pub backing_image_checksum: String,
    pub id: String,
    pub name: String,
    pub labels: HashMap<String, String>,
    pub messages: HashMap<String, String>,
    #[serde(rename = "storageClassName")]
    pub storage_class_name: String,
    pub created: String,
    pub links: HashMap<String, String>,
    pub r#type: String,
    pub actions: HashMap<String, String>,
    #[serde(rename = "backingImageName")]
    pub backing_image_name: String,
    pub size: String,
    #[serde(rename = "lastBackupName")]
    pub last_backup_name: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Setting {
    pub links: HashMap<String, String>,
    pub actions: HashMap<String, String>,
    pub id: String,
    pub r#type: String,
    pub definition: SettingDefinition,
    pub value: String,
    pub name: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RecurringJob {
    pub r#type: String,
    pub retain: i32,
    pub links: HashMap<String, String>,
    pub task: String,
    pub groups: Vec<String>,
    pub actions: HashMap<String, String>,
    pub cron: String,
    pub labels: HashMap<String, String>,
    pub concurrency: i32,
    pub id: String,
    pub name: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct EngineImage {
    #[serde(rename = "cliAPIVersion")]
    pub cli_api_version: i32,
    #[serde(rename = "ownerID")]
    pub owner_id: String,
    pub r#type: String,
    #[serde(rename = "nodeDeploymentMap")]
    pub node_deployment_map: HashMap<String, String>,
    #[serde(rename = "refCount")]
    pub ref_count: i32,
    #[serde(rename = "controllerAPIVersion")]
    pub controller_api_version: i32,
    pub conditions: Vec<String>,
    #[serde(rename = "cliAPIMinVersion")]
    pub cli_api_min_version: i32,
    pub version: String,
    pub actions: HashMap<String, String>,
    pub default: bool,
    #[serde(rename = "buildDate")]
    pub build_date: String,
    #[serde(rename = "noRefSince")]
    pub no_ref_since: String,
    #[serde(rename = "dataFormatVersion")]
    pub data_format_version: i32,
    pub incompatible: bool,
    #[serde(rename = "gitCommit")]
    pub git_commit: String,
    pub state: String,
    #[serde(rename = "controllerAPIMinVersion")]
    pub controller_api_min_version: i32,
    pub id: String,
    pub image: String,
    #[serde(rename = "dataFormatMinVersion")]
    pub data_format_min_version: i32,
    pub links: HashMap<String, String>,
    pub name: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BackingImage {
    pub actions: HashMap<String, String>,
    pub id: String,
    pub parameters: HashMap<String, String>,
    pub name: String,
    pub uuid: String,
    pub size: i32,
    #[serde(rename = "currentChecksum")]
    pub current_checksum: String,
    #[serde(rename = "diskFileStatusMap")]
    pub disk_file_status_map: HashMap<String, BackingImageDiskFileStatus>,
    #[serde(rename = "expectedChecksum")]
    pub expected_checksum: String,
    #[serde(rename = "deletionTimestamp")]
    pub deletion_timestamp: String,
    pub links: HashMap<String, String>,
    pub r#type: String,
    #[serde(rename = "sourceType")]
    pub source_type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Node {
    pub address: String,
    #[serde(rename = "allowScheduling")]
    pub allow_scheduling: bool,
    #[serde(rename = "evictionRequested")]
    pub eviction_requested: bool,
    #[serde(rename = "instanceManagerCPURequest")]
    pub instance_manager_cpu_request: i32,
    pub id: String,
    pub actions: HashMap<String, String>,
    #[serde(rename = "autoEvicting")]
    pub auto_evicting: bool,
    pub r#type: String,
    pub zone: String,
    pub tags: Vec<String>,
    pub links: HashMap<String, String>,
    pub region: String,
    pub conditions: HashMap<String, NodeCondition>,
    pub disks: HashMap<String, DiskInfo>,
    pub name: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DiskUpdateInput {
    pub disks: Vec<DiskUpdate>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DiskInfo {
    #[serde(rename = "storageAvailable")]
    pub storage_available: i32,
    #[serde(rename = "storageMaximum")]
    pub storage_maximum: i32,
    #[serde(rename = "storageReserved")]
    pub storage_reserved: i32,
    #[serde(rename = "diskType")]
    pub disk_type: String,
    #[serde(rename = "diskUUID")]
    pub disk_uuid: String,
    #[serde(rename = "storageScheduled")]
    pub storage_scheduled: i32,
    pub tags: Vec<String>,
    pub path: String,
    #[serde(rename = "scheduledReplica")]
    pub scheduled_replica: HashMap<String, String>,
    #[serde(rename = "evictionRequested")]
    pub eviction_requested: bool,
    #[serde(rename = "allowScheduling")]
    pub allow_scheduling: bool,
    pub conditions: HashMap<String, DiskCondition>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct KubernetesStatus {
    #[serde(rename = "pvStatus")]
    pub pv_status: String,
    #[serde(rename = "workloadsStatus")]
    pub workloads_status: Vec<WorkloadStatus>,
    #[serde(rename = "lastPodRefAt")]
    pub last_pod_ref_at: String,
    #[serde(rename = "pvName")]
    pub pv_name: String,
    #[serde(rename = "lastPVCRefAt")]
    pub last_pvc_ref_at: String,
    #[serde(rename = "pvcName")]
    pub pvc_name: String,
    pub namespace: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BackupListOutput {
    pub data: Vec<Backup>,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SnapshotListOutput {
    pub data: Vec<Snapshot>,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SystemBackup {
    pub state: String,
    pub id: String,
    pub name: String,
    pub version: String,
    #[serde(rename = "volumeBackupPolicy")]
    pub volume_backup_policy: String,
    pub error: String,
    pub actions: HashMap<String, String>,
    pub links: HashMap<String, String>,
    pub r#type: String,
    #[serde(rename = "createdAt")]
    pub created_at: String,
    #[serde(rename = "managerImage")]
    pub manager_image: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SystemRestore {
    pub error: String,
    pub actions: HashMap<String, String>,
    pub name: String,
    pub state: String,
    pub r#type: String,
    pub links: HashMap<String, String>,
    #[serde(rename = "systemBackup")]
    pub system_backup: String,
    #[serde(rename = "createdAt")]
    pub created_at: String,
    pub id: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SnapshotCrListOutput {
    pub r#type: String,
    pub data: Vec<SnapshotCr>,
}
