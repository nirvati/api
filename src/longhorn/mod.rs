use crate::longhorn::types::Volume;

pub mod types;

pub struct Client {
    base_url: String,
    reqwest_client: reqwest::Client,
}

#[derive(Debug, serde::Deserialize)]
struct Volumes {
    data: Vec<Volume>,
}

impl Client {
    pub fn new(base_url: &str) -> Self {
        Client {
            base_url: base_url.to_string(),
            reqwest_client: reqwest::Client::new(),
        }
    }

    async fn get<T: serde::de::DeserializeOwned>(&self, path: &str) -> Result<T, reqwest::Error> {
        let url = format!("{}/{}", self.base_url, path);
        self.reqwest_client.get(&url).send().await?.json().await
    }

    async fn post<T: serde::de::DeserializeOwned, B: serde::Serialize>(
        &self,
        path: &str,
        body: &B,
    ) -> Result<T, reqwest::Error> {
        let url = format!("{}/{}", self.base_url, path);
        self.reqwest_client
            .post(&url)
            .json(body)
            .send()
            .await?
            .json()
            .await
    }

    /*async fn put<T: serde::de::DeserializeOwned, B: serde::Serialize>(
        &self,
        path: &str,
        body: &B,
    ) -> Result<T, reqwest::Error> {
        let url = format!("{}/{}", self.base_url, path);
        self.reqwest_client
            .put(&url)
            .json(body)
            .send()
            .await?
            .json()
            .await
    }

    async fn delete<T: serde::de::DeserializeOwned>(&self, path: &str) -> Result<T, reqwest::Error> {
        let url = format!("{}/{}", self.base_url, path);
        self.reqwest_client.delete(&url).send().await?.json().await
    }*/

    pub async fn get_volumes(&self) -> Result<Vec<Volume>, reqwest::Error> {
        self.get::<Volumes>("v1/volumes").await.map(|v| v.data)
    }

    pub async fn get_volume(&self, name: &str) -> Result<Volume, reqwest::Error> {
        self.get::<Volume>(&format!("v1/volumes/{}", name)).await
    }

    pub async fn update_replica_count(
        &self,
        name: &str,
        replica_count: u8,
    ) -> Result<Volume, reqwest::Error> {
        self.post(
            &format!("v1/volumes/{}?action=updateReplicaCount", name),
            &serde_json::json!({ "replicaCount": replica_count }),
        )
        .await
    }

    pub async fn expand(&self, name: &str, size: u64) -> Result<Volume, reqwest::Error> {
        self.post(
            &format!("v1/volumes/{}?action=expand", name),
            &serde_json::json!({ "size": size.to_string() }),
        )
        .await
    }
}
