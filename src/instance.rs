use std::str::FromStr;
use std::sync::OnceLock;

use async_graphql::Enum;

pub static INSTANCE_TYPE: OnceLock<InstanceType> = OnceLock::new();

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Enum)]
pub enum InstanceType {
    // Lite is for personal use on devices with limited resources, such as a Raspberry Pi 4 or the Radxa CM3
    // It does not use Longhorn, so it can only support a single disk & server
    LITE,
    // Home is for personal use on devices with more resources, such as a Raspberry Pi 5, Nabi hardware, or Radxa CM5
    // It uses Longhorn for storage and a single postgres database, so it supports multi-server, but does not have the
    // highest performance
    HOME,
    // Enterprise is for larger, distributed deployments
    // It uses a distributed PostgreSQL database (cloudNativePg)
    ENTERPRISE,
}

impl FromStr for InstanceType {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "lite" => Ok(InstanceType::LITE),
            "home" => Ok(InstanceType::HOME),
            "enterprise" => Ok(InstanceType::ENTERPRISE),
            _ => Err(format!("{} is not a valid instance type", s)),
        }
    }
}
