use async_graphql::{EmptySubscription, Schema};

use nirvati_api::graphql::Mutation;
use nirvati_api::graphql::Query;

fn main() {
    let schema = Schema::build(Query, Mutation::default(), EmptySubscription).finish();
    std::fs::write("schema.gql", schema.sdl()).expect("Failed to save schema");
}
