use async_graphql::{Context, Error, Object, Result};
use biscuit::jwa::*;
use biscuit::jws::*;
use biscuit::*;
use tonic::Request;

use crate::api::core::{AddUserAuthRequest, AddUserNsRequest, DeleteUserAuthRequest};
use crate::graphql::auth_ctx::UserCtx;
use crate::graphql::auth_guard::PermissionGuard;
use crate::graphql::clients::{manage_client, middlewares_client, users_client};
use crate::graphql::query::users::Permission;
use crate::prisma;
use crate::prisma::Permission as PrismaPermission;
use crate::prisma::PrismaClient;
use crate::utils::email::validate_email;
use crate::utils::jwts::JwtAdditionalClaims;

#[derive(Default)]
pub struct User;

#[Object(name = "UserMutations")]
impl User {
    #[graphql(guard = "PermissionGuard::new(PrismaPermission::ManageUsers)")]
    async fn create_user(
        &self,
        ctx: &Context<'_>,
        username: String,
        password: String,
        name: String,
        email: Option<String>,
        permissions: Option<Vec<Permission>>,
        group: Option<String>,
    ) -> Result<bool> {
        let user = ctx.data::<UserCtx>()?;
        if !user.permissions.has(PrismaPermission::ManageGroups)
            && group.as_ref().is_some_and(|g| g != &user.user_group)
        {
            return Err(Error::new("Forbidden"));
        }
        // TODO: Technically, ManageUsers allows privilege escalation by changing the password of a user with higher permissions
        // But we're not going to worry about that for now
        for permission in &permissions.clone().unwrap_or_default() {
            if !user.permissions.has((*permission).into()) {
                return Err(Error::new("Forbidden"));
            }
        }
        let user_group = group.unwrap_or_else(|| user.user_group.clone());
        let client = ctx.data::<PrismaClient>()?;
        let total = client
            .user()
            .count(vec![])
            .exec()
            .await
            .map_err(|_| Error::new("Failed to count users"))? as usize;
        if total > 0 {
            return Err(Error::new("Initial user already exists"));
        }
        if let Some(email) = &email {
            let is_valid = validate_email(email);
            if !is_valid {
                return Err(Error::new("Invalid email"));
            }
        }
        client
            .user()
            .create(
                username.clone(),
                name,
                bcrypt::hash(password, bcrypt::DEFAULT_COST)
                    .map_err(|_| Error::new("Failed to hash password"))?,
                prisma::user_group::UniqueWhereParam::NameEquals(user_group.clone()),
                vec![
                    prisma::user::SetParam::SetEmail(email),
                    prisma::user::SetParam::SetPermissions(
                        permissions
                            .unwrap_or_default()
                            .into_iter()
                            .map(Into::into)
                            .collect(),
                    ),
                ],
            )
            .exec()
            .await
            .map_err(|_| Error::new("Failed to create user"))?;
        let mut manage_client = manage_client(ctx)?;
        manage_client
            .preload_apps(tonic::Request::new(crate::api::apps::PreloadAppsRequest {
                user: user_group,
            }))
            .await
            .map_err(|_| Error::new("Failed to preload apps"))?;
        let mut users_client = users_client(ctx)?;
        users_client
            .create_user_namespace(tonic::Request::new(AddUserNsRequest { user: username }))
            .await
            .map_err(|_| Error::new("Failed to create user in manager"))?;
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(PrismaPermission::ManageUsers)")]
    async fn update_user(
        &self,
        ctx: &Context<'_>,
        username: Option<String>,
        password: Option<String>,
        name: Option<String>,
        email: Option<String>,
        erase_email: Option<bool>,
        permissions: Option<Vec<Permission>>,
        group: Option<String>,
    ) -> Result<bool> {
        let user = ctx.data::<UserCtx>()?;
        let username = username.unwrap_or(user.username.clone());
        if username != user.username
            && !user.permissions.has(PrismaPermission::ManageGroups)
            && group.as_ref().is_some_and(|g| g != &user.user_group)
        {
            return Err(Error::new("Forbidden"));
        }
        let client = ctx.data::<PrismaClient>()?;
        let current_user = client
            .user()
            .find_unique(prisma::user::UniqueWhereParam::UsernameEquals(
                username.clone(),
            ))
            .exec()
            .await
            .map_err(|_| Error::new("Failed to find user"))?
            .ok_or(Error::new("Failed to find user"))?;
        if current_user.user_group_id != user.user_group
            && !user.permissions.has(PrismaPermission::ManageGroups)
        {
            return Err(Error::new("Forbidden"));
        }
        let mut settings = vec![];

        if let Some(name) = name {
            settings.push(prisma::user::SetParam::SetName(name));
        }
        if erase_email.unwrap_or(false) && email.is_none() {
            settings.push(prisma::user::SetParam::SetEmail(None));
        } else if let Some(email) = email {
            settings.push(prisma::user::SetParam::SetEmail(Some(email)));
        }

        if let Some(password) = password {
            settings.push(prisma::user::SetParam::SetPassword(
                bcrypt::hash(&password, bcrypt::DEFAULT_COST)
                    .map_err(|_| Error::new("Failed to hash password"))?,
            ));
            let mut mw_client = middlewares_client(ctx)?;
            mw_client
                .add_user_auth(Request::new(AddUserAuthRequest {
                    user: username.clone(),
                    password,
                }))
                .await?;
        }
        if let Some(permissions) = permissions {
            for permission in &permissions {
                if !user.permissions.has((*permission).into()) {
                    return Err(Error::new("Forbidden"));
                }
            }
            settings.push(prisma::user::SetParam::SetPermissions(
                permissions.into_iter().map(Into::into).collect(),
            ));
        }
        if let Some(group) = group {
            settings.push(prisma::user::SetParam::ConnectUserGroup(
                prisma::user_group::UniqueWhereParam::NameEquals(group),
            ));
        }

        client
            .user()
            .update(
                prisma::user::UniqueWhereParam::UsernameEquals(username),
                settings,
            )
            .exec()
            .await
            .map_err(|_| Error::new("Failed to update user"))?;

        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(PrismaPermission::ManageUsers)")]
    async fn delete_user(&self, ctx: &Context<'_>, username: String) -> Result<bool> {
        let user = ctx.data::<UserCtx>()?;
        let client = ctx.data::<PrismaClient>()?;
        let current_user = client
            .user()
            .find_unique(prisma::user::UniqueWhereParam::UsernameEquals(
                username.clone(),
            ))
            .exec()
            .await
            .map_err(|_| Error::new("Failed to find user"))?
            .ok_or(Error::new("Failed to find user"))?;
        if current_user.user_group_id != user.user_group
            && !user.permissions.has(PrismaPermission::ManageGroups)
        {
            return Err(Error::new("Forbidden"));
        }
        client
            .user()
            .delete(prisma::user::UniqueWhereParam::UsernameEquals(
                username.clone(),
            ))
            .exec()
            .await
            .map_err(|_| Error::new("Failed to delete user"))?;
        let mut users_client = users_client(ctx)?;
        users_client
            .delete_user_namespace(Request::new(crate::api::core::DeleteUserNsRequest {
                user: username.clone(),
            }))
            .await
            .map_err(|_| Error::new("Failed to delete user in manager"))?;
        let mut mw_client = middlewares_client(ctx)?;
        mw_client
            .remove_user_auth(Request::new(DeleteUserAuthRequest { user: username }))
            .await?;
        // TODO: Also delete apps dir
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(PrismaPermission::ManageGroups)")]
    async fn create_user_group(&self, ctx: &Context<'_>, name: String) -> Result<bool> {
        let client = ctx.data::<PrismaClient>()?;
        client
            .user_group()
            .create(name, vec![])
            .exec()
            .await
            .map_err(|_| Error::new("Failed to create user group"))?;
        Ok(true)
    }

    async fn login(&self, ctx: &Context<'_>, username: String, password: String) -> Result<String> {
        let client = ctx.data::<PrismaClient>()?;
        let user = client
            .user()
            .find_unique(prisma::user::UniqueWhereParam::UsernameEquals(
                username.clone(),
            ))
            .exec()
            .await
            .map_err(|_| Error::new("Failed to find user"))?
            .ok_or(Error::new("Failed to find user"))?;
        if bcrypt::verify(password, &user.password)
            .map_err(|_| Error::new("Failed to verify password"))?
        {
            let secret = ctx.data_unchecked::<Secret>();
            let claims = ClaimsSet::<JwtAdditionalClaims> {
                registered: RegisteredClaims {
                    issuer: Some("nirvati".to_string()),
                    subject: Some(username),
                    issued_at: Some(chrono::Utc::now().into()),
                    expiry: Some(
                        // TODO: Make expiry configurable
                        (chrono::Utc::now() + chrono::Duration::minutes(10)).into(),
                    ),
                    ..Default::default()
                },
                private: JwtAdditionalClaims {
                    user_group: user.user_group_id,
                    permission_set: user.permissions.as_slice().into(),
                    email: user.email,
                },
            };
            let token = JWT::new_decoded(
                From::from(RegisteredHeader {
                    algorithm: SignatureAlgorithm::HS256,
                    ..Default::default()
                }),
                claims.clone(),
            );

            let token = token
                .into_encoded(secret)
                .map_err(|_| Error::new("Failed to encode token"))?;
            Ok(token.unwrap_encoded().to_string())
        } else {
            Err(Error::new("Invalid password"))
        }
    }
}
