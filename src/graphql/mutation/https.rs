use async_graphql::{Context, Error, Object, Result};
use tonic::Request;

use crate::api::apps::{AddDomainRequest, RemoveDomainRequest};
use crate::api::https::{AddCertificateRequest, BuiltInAcmeProvider, DeleteCertificateRequest};
use crate::graphql::auth_ctx::UserCtx;
use crate::graphql::auth_guard::PermissionGuard;
use crate::graphql::clients::{apps_client, https_client};
use crate::graphql::query::https::acme_provider::AcmeProvider;
use crate::graphql::query::https::user_domain::DnsProvider;
use crate::prisma::{Permission, PrismaClient};
use crate::utils::user_state::get_user_state;
use crate::{api, prisma};

#[derive(Default)]
pub struct Https;

impl From<prisma::AcmeProvider> for BuiltInAcmeProvider {
    fn from(value: prisma::AcmeProvider) -> Self {
        match value {
            prisma::AcmeProvider::LetsEncrypt => BuiltInAcmeProvider::LetsEncrypt,
            prisma::AcmeProvider::BuyPass => BuiltInAcmeProvider::BuyPass,
        }
    }
}

pub async fn add_app_domain(
    ctx: &Context<'_>,
    app: String,
    domain: String,
    acme_provider: AcmeProvider,
) -> Result<bool> {
    let user_id = ctx.data_unchecked::<UserCtx>().username.clone();
    let client = ctx.data_unchecked::<PrismaClient>();
    let mut apps_client = apps_client(ctx)?;
    let mut https_client = https_client(ctx)?;
    let acme_account = client
        .acme_account()
        .find_unique(
            prisma::acme_account::UniqueWhereParam::ProviderOwnerNameEquals(
                acme_provider.into(),
                user_id.clone(),
            ),
        )
        .select(prisma::acme_account::select!({ id }))
        .exec()
        .await
        .map_err(|_| "Failed to find ACME account for this provider, is it enabled?")?
        .ok_or::<Error>(
            "Failed to find ACME account for this provider, is it enabled?".into(),
        )?;
    let sld = domain.split('.').rev().take(2).collect::<Vec<_>>();
    let sld = sld.into_iter().rev().collect::<Vec<_>>().join(".");
    let mut potential_parents = client
        .domain()
        .find_many(vec![
            prisma::domain::owner_name::equals(user_id.clone()),
            prisma::domain::name::ends_with(sld),
        ])
        .select(prisma::domain::select!({
                id
                name
            }))
        .exec()
        .await
        .map_err::<Error, _>(|_| "Failed to get potential parent domains from DB".into())?;
    potential_parents.sort_by(|potential_parent_a, potential_parent_b| {
        potential_parent_a
            .name
            .len()
            .cmp(&potential_parent_b.name.len())
    });
    apps_client
        .add_domain(Request::new(AddDomainRequest {
            user_state: get_user_state(ctx).await,
            id: app.clone(),
            domain: domain.clone(),
        }))
        .await
        .map_err::<Error, _>(|_| "Failed to add domain to app".into())?;
    https_client
        .add_certificate(Request::new(AddCertificateRequest {
            user: user_id.clone(),
            namespace: format!("{}-{}", user_id, app),
            domain: domain.clone(),
            acme_provider: Some(acme_provider.into()),
            parent: potential_parents.last().map(|p| p.name.clone()),
        }))
        .await
        .map_err::<Error, _>(|_| "Failed to add app certificate".into())?;
    if let Some(parent) = potential_parents.last() {
        client
            .app_domain()
            .create(
                domain,
                prisma::app_installation::UniqueWhereParam::AppIdOwnerNameEquals(
                    app.clone(),
                    user_id.clone(),
                ),
                prisma::acme_account::UniqueWhereParam::IdEquals(acme_account.id),
                vec![prisma::app_domain::SetParam::ConnectParentDomain(
                    prisma::domain::UniqueWhereParam::IdEquals(parent.id.clone()),
                )],
            )
            .exec()
            .await
            .map_err::<Error, _>(|_| "Failed to create app domain".into())?;
    } else {
        client
            .app_domain()
            .create(
                domain,
                prisma::app_installation::UniqueWhereParam::AppIdOwnerNameEquals(
                    app.clone(),
                    user_id.clone(),
                ),
                prisma::acme_account::UniqueWhereParam::IdEquals(acme_account.id),
                vec![],
            )
            .exec()
            .await
            .map_err::<Error, _>(|_| "Failed to create app domain".into())?;
    }
    Ok(true)
}

#[Object(name = "HttpsMutations")]
impl Https {
    #[graphql(guard = "PermissionGuard::new(Permission::ManageIssuers)")]
    async fn create_acme_issuer(
        &self,
        ctx: &Context<'_>,
        provider: AcmeProvider,
        share_email: Option<bool>,
    ) -> Result<bool> {
        let user_id = ctx.data_unchecked::<UserCtx>().username.clone();
        let client = ctx.data_unchecked::<PrismaClient>();
        let share_email = share_email.unwrap_or(false);
        if share_email && ctx.data_unchecked::<UserCtx>().email.is_none() {
            return Err(Error::new(
                "You must have set an email address to share it with ACME providers",
            ));
        };
        if provider == AcmeProvider::BuyPass && !share_email {
            return Err(Error::new(
                "You must share your email address with BuyPass to use it as an ACME provider",
            ));
        }
        client
            .acme_account()
            .create(
                provider.into(),
                prisma::user::UniqueWhereParam::UsernameEquals(user_id.clone()),
                vec![prisma::acme_account::SetParam::SetShareEmail(true)],
            )
            .exec()
            .await
            .map_err(|_| Error::new("Failed to create ACME account"))?;
        let mut https_client = https_client(ctx)?;
        https_client
            .add_issuer(Request::new(api::https::AddUserIssuerRequest {
                user: user_id.clone(),
                provider: Some(provider.into()),
                email: if share_email {
                    ctx.data_unchecked::<UserCtx>().email.clone()
                } else {
                    None
                },
            }))
            .await
            .map_err::<Error, _>(|_| "Failed to add issuer".into())?;
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageIssuers)")]
    async fn delete_acme_issuer(&self, ctx: &Context<'_>, provider: AcmeProvider) -> Result<bool> {
        let user_id = ctx.data_unchecked::<UserCtx>().username.clone();
        let client = ctx.data_unchecked::<PrismaClient>();
        client
            .acme_account()
            .delete(
                prisma::acme_account::UniqueWhereParam::ProviderOwnerNameEquals(
                    provider.into(),
                    user_id.clone(),
                ),
            )
            .exec()
            .await
            .map_err(|_| Error::new("Failed to delete ACME account"))?;
        let mut https_client = https_client(ctx)?;
        https_client
            .delete_issuer(Request::new(api::https::DeleteUserIssuerRequest {
                user: user_id,
                provider: Some(provider.into()),
            }))
            .await
            .map_err::<Error, _>(|_| "Failed to add issuer".into())?;
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageApps)")]
    async fn add_app_domain(
        &self,
        ctx: &Context<'_>,
        app: String,
        domain: String,
        acme_provider: AcmeProvider,
    ) -> Result<bool> {
        add_app_domain(ctx, app, domain, acme_provider).await
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageApps)")]
    async fn remove_app_domain(&self, ctx: &Context<'_>, domain: String) -> Result<bool> {
        let user_id = ctx.data_unchecked::<UserCtx>().username.clone();
        let client = ctx.data_unchecked::<PrismaClient>();
        let mut https_client = https_client(ctx)?;
        let current_entry = client
            .app_domain()
            .count(vec![
                prisma::app_domain::name::equals(domain.clone()),
                prisma::app_domain::WhereParam::AppInstallationIs(vec![
                    prisma::app_installation::owner_name::equals(user_id.clone()),
                ]),
            ])
            .exec()
            .await
            .map_err::<Error, _>(|e| e.into())?;
        if current_entry == 0 {
            return Err(Error::new("Domain not found"));
        }
        let deleted = client
            .app_domain()
            .delete(prisma::app_domain::UniqueWhereParam::NameEquals(
                domain.clone(),
            ))
            .select(prisma::app_domain::select!({
                app_installation: select {
                    app_id
                }
            }))
            .exec()
            .await
            .map_err::<Error, _>(|e| e.into())?;
        https_client
            .remove_certificate(Request::new(DeleteCertificateRequest {
                namespace: format!("{}-{}", user_id, deleted.app_installation.app_id),
                domain: domain.clone(),
            }))
            .await
            .map_err::<Error, _>(|_| "Failed to remove app certificate".into())?;
        let mut apps_client = apps_client(ctx)?;
        apps_client
            .remove_domain(Request::new(RemoveDomainRequest {
                user_state: get_user_state(ctx).await,
                id: deleted.app_installation.app_id,
                domain,
            }))
            .await
            .map_err::<Error, _>(|_| "Failed to remove domain from app".into())?;
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageDomains)")]
    async fn add_domain(
        &self,
        ctx: &Context<'_>,
        domain: String,
        dns_provider: Option<DnsProvider>,
        dns_auth: Option<String>,
        acme_providers: Vec<AcmeProvider>,
    ) -> Result<bool> {
        let user_id = ctx.data_unchecked::<UserCtx>().username.clone();
        let user_email = ctx.data_unchecked::<UserCtx>().email.clone();
        let db = ctx.data_unchecked::<PrismaClient>();
        let dns_provider = dns_provider.unwrap_or(DnsProvider::None);
        let new_entry = db
            .domain()
            .create(
                domain.clone(),
                prisma::user::UniqueWhereParam::UsernameEquals(user_id.clone()),
                dns_provider.into(),
                vec![prisma::domain::SetParam::SetProviderAuth(dns_auth.clone())],
            )
            .exec()
            .await
            .map_err::<Error, _>(|e| e.into())?;
        tracing::info!("Created domain");
        db.domain()
            .update(
                prisma::domain::UniqueWhereParam::IdEquals(new_entry.id.clone()),
                vec![prisma::domain::SetParam::ConnectAcmeAccounts(
                    acme_providers
                        .iter()
                        .map(|provider| {
                            prisma::acme_account::UniqueWhereParam::ProviderOwnerNameEquals(
                                (*provider).into(),
                                user_id.clone(),
                            )
                        })
                        .collect::<Vec<_>>(),
                )],
            )
            .exec()
            .await
            .map_err::<Error, _>(|e| e.into())?;
        let acme_accounts = db
            .domain()
            .find_unique(prisma::domain::UniqueWhereParam::IdEquals(new_entry.id))
            .select(prisma::domain::select!({
                acme_accounts: select {
                    provider
                    share_email
                }
            }))
            .exec()
            .await
            .map_err::<Error, _>(|e| e.into())?
            .ok_or::<Error>("Failed to find domain".into())?;
        let mut https_client = https_client(ctx)?;
        let dns_provider: api::https::DnsProvider = dns_provider.into();
        for acme_provider in acme_accounts.acme_accounts {
            let api_acme_provider: api::https::BuiltInAcmeProvider = acme_provider.provider.into();
            let share_email = acme_provider.share_email;
            if share_email && user_email.is_none() {
                return Err(Error::new(
                    "You must have set an email address to share it with ACME providers",
                ));
            };
            https_client
                .add_domain_issuer(Request::new(api::https::CreateDomainIssuerRequest {
                    user: user_id.clone(),
                    domain: domain.clone(),
                    dns_provider: dns_provider.into(),
                    auth: dns_auth.clone(),
                    email: if share_email {
                        user_email.clone()
                    } else {
                        None
                    },
                    acme_provider: Some(api::https::AcmeProvider {
                        provider: Some(api::https::acme_provider::Provider::BuiltIn(
                            api_acme_provider.into(),
                        )),
                    }),
                }))
                .await
                .map_err::<Error, _>(|_| "Failed to add domain issuer".into())?;
        }
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageDomains)")]
    async fn remove_domain(&self, ctx: &Context<'_>, domain: String) -> Result<bool> {
        let user_id = ctx.data_unchecked::<UserCtx>().username.clone();
        let db = ctx.data_unchecked::<PrismaClient>();
        let deleted = db
            .domain()
            .delete(prisma::domain::UniqueWhereParam::NameOwnerNameEquals(
                domain.clone(),
                user_id.clone(),
            ))
            .select(prisma::domain::select!({
                acme_accounts: select {
                    provider
                }
            }))
            .exec()
            .await
            .map_err::<Error, _>(|e| e.into())?;
        let mut https_client = https_client(ctx)?;
        for acme_provider in deleted.acme_accounts {
            let api_acme_provider: BuiltInAcmeProvider = acme_provider.provider.into();
            https_client
                .remove_domain_issuer(Request::new(api::https::DeleteDomainIssuerRequest {
                    user: user_id.clone(),
                    domain: domain.clone(),
                    acme_provider: Some(api::https::AcmeProvider {
                        provider: Some(api::https::acme_provider::Provider::BuiltIn(
                            api_acme_provider.into(),
                        )),
                    }),
                }))
                .await
                .map_err::<Error, _>(|_| "Failed to remove domain issuer".into())?;
        }
        Ok(true)
    }
}
