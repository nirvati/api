use async_graphql::{Context, Error, Json, Object, Result};
use prisma_client_rust::Direction;
use tonic::Request;

use crate::api::apps::download_apps_request::Reference;
use crate::api::apps::{
    AddDomainRequest, AddStoreRequest, DownloadAppsRequest, GenerateFilesRequest, InstallRequest,
    RemoveStoreRequest, UninstallRequest,
};
use crate::graphql::auth_ctx::UserCtx;
use crate::graphql::auth_guard::PermissionGuard;
use crate::graphql::clients::{apps_client, manage_client};
use crate::graphql::mutation::https::add_app_domain;
use crate::graphql::query::apps::store::StoreType;
use crate::graphql::query::https::acme_provider::AcmeProvider;
use crate::prisma;
use crate::prisma::{app_installation, Permission, PrismaClient};
use crate::utils::user_state::get_user_state;

#[derive(Default)]
pub struct Apps;

#[Object(name = "AppMutations")]
impl Apps {
    #[graphql(guard = "PermissionGuard::new(Permission::ManageApps)")]
    async fn install_app(
        &self,
        ctx: &Context<'_>,
        app_id: String,
        settings: Json<serde_json::Value>,
        initial_domain: String,
        acme_provider: AcmeProvider,
    ) -> Result<bool> {
        if app_id.as_str() == "nirvati" {
            return Err(Error::new(
                "Cannot install Nirvati, it is already provided as a system component!",
            ));
        };
        // We can safely unwrap here because we know the user is logged in
        let user = ctx.data_unchecked::<UserCtx>().username.clone();
        let mut client = apps_client(ctx)?;
        let db = ctx.data_unchecked::<PrismaClient>();
        let user_state = get_user_state(ctx).await;
        client
            .apply(Request::new(InstallRequest {
                id: app_id.clone(),
                user_state: user_state.clone(),
                initial_domain: Some(initial_domain.clone()),
            }))
            .await
            .map_err(|_| Error::new("Failed to install app"))?;

        db.app_installation()
            .create(
                app_id.clone(),
                prisma::user::UniqueWhereParam::UsernameEquals(user),
                (*settings).clone(),
                vec![],
            )
            .exec()
            .await
            .map_err(|_| Error::new("Failed to create app installation"))?;
        add_app_domain(ctx, app_id, initial_domain, acme_provider).await
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageApps)")]
    async fn uninstall_app(&self, ctx: &Context<'_>, app_id: String) -> Result<bool> {
        // We can safely unwrap here because we know the user is logged in
        let user = ctx.data_unchecked::<UserCtx>().username.clone();
        if app_id.as_str() == "nirvati" {
            return Err(Error::new("Cannot uninstall Nirvati!"));
        } else if app_id.as_str() == "chartmuseum" && user.as_str() == "admin" {
            return Err(Error::new(
                "Cannot uninstall Chartmuseum, it is a built-in component!",
            ));
        };
        let mut client = apps_client(ctx)?;
        let db = ctx.data_unchecked::<PrismaClient>();
        client
            .uninstall(Request::new(UninstallRequest {
                id: app_id.clone(),
                user_state: get_user_state(ctx).await,
            }))
            .await
            .map_err(|_| Error::new("Failed to uninstall app"))?;
        db.app_installation()
            .delete(app_installation::UniqueWhereParam::AppIdOwnerNameEquals(
                app_id,
                user.clone(),
            ))
            .exec()
            .await
            .map_err(|_| Error::new("Failed to delete app installation"))
            .map(|_| true)
        // We do not need to delete domains via the HTTPS API because they are stored in the app namespace,
        // which is deleted when the app is uninstalled
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageApps)")]
    async fn generate(&self, ctx: &Context<'_>) -> Result<bool> {
        let mut client = manage_client(ctx)?;
        client
            .generate_files(Request::new(GenerateFilesRequest {
                user_state: get_user_state(ctx).await,
            }))
            .await
            .map_err(|_| Error::new("Failed to generate apps"))
            .map(|_| true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageAppStores)")]
    async fn add_app_store(
        &self,
        ctx: &Context<'_>,
        src: String,
        store_type: StoreType,
    ) -> Result<bool> {
        let mut client = manage_client(ctx)?;
        let store_type: crate::api::apps::StoreType = store_type.into();
        client
            .add_store(Request::new(AddStoreRequest {
                src,
                user_state: get_user_state(ctx).await,
                r#type: store_type as i32,
                // TODO: Add plugin support
                store_plugin: None,
            }))
            .await
            .map_err(|_| Error::new("Failed to add app store"))
            .map(|_| true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageAppStores)")]
    async fn remove_app_store(&self, ctx: &Context<'_>, src: String) -> Result<bool> {
        let mut client = manage_client(ctx)?;
        client
            .remove_store(Request::new(RemoveStoreRequest {
                store_src: src,
                user_state: get_user_state(ctx).await,
            }))
            .await
            .map_err(|_| Error::new("Failed to remove app store"))
            .map(|_| true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageApps)")]
    async fn update_app(&self, ctx: &Context<'_>, app: String) -> Result<bool> {
        let mut client = manage_client(ctx)?;
        client
            .download(Request::new(DownloadAppsRequest {
                user_state: get_user_state(ctx).await,
                reference: Some(Reference::App(app.clone())),
            }))
            .await
            .map_err(|_| Error::new("Failed to download new app"))?;
        client
            .generate_files(Request::new(GenerateFilesRequest {
                user_state: get_user_state(ctx).await,
            }))
            .await
            .map_err(|_| Error::new("Failed to regenerate files"))?;
        let db = ctx.data_unchecked::<PrismaClient>();
        let app_domains = db
            .app_domain()
            .find_many(vec![prisma::app_domain::WhereParam::AppInstallationIs(
                vec![prisma::app_installation::app_id::equals(app.clone())],
            )])
            .order_by(prisma::app_domain::created_at::order(Direction::Asc))
            .select(prisma::app_domain::select!({ name }))
            .exec()
            .await
            .map_err(|_| Error::new("Failed to get app domains"))?;
        let mut client = apps_client(ctx)?;
        let mut oldest_app_domain = None;
        for domain in app_domains {
            if oldest_app_domain.is_none() {
                oldest_app_domain = Some(domain.name.clone());
            }
            client
                .add_domain(Request::new(AddDomainRequest {
                    user_state: get_user_state(ctx).await,
                    id: app.clone(),
                    domain: domain.name,
                }))
                .await
                .map_err(|_| Error::new("Failed to update app ingress"))?;
        }
        client
            .apply(Request::new(InstallRequest {
                user_state: get_user_state(ctx).await,
                id: app,
                initial_domain: oldest_app_domain,
            }))
            .await
            .map_err(|_| Error::new("Failed to download new app"))
            .map(|_| true)
    }
}
