use async_graphql::{Error, Object, Result};
use tonic::Request;

use crate::api::core::{AddProxyRequest, HttpsOptions, RemoveProxyRequest, Route};
use crate::api::https::{AddCertificateRequest, DeleteCertificateRequest};
use crate::graphql::auth_ctx::UserCtx;
use crate::graphql::auth_guard::PermissionGuard;
use crate::graphql::clients::{https_client, proxies_client};
use crate::graphql::query::https::acme_provider::AcmeProvider;
use crate::graphql::query::https::proxy::Protocol;
use crate::prisma;
use crate::prisma::{acme_account, proxy, route, service, user, Permission};

#[derive(Default)]
pub struct Proxies;

impl From<prisma::Protocol> for i32 {
    fn from(p: prisma::Protocol) -> Self {
        match p {
            prisma::Protocol::Http => 0,
            prisma::Protocol::Https => 1,
        }
    }
}

async fn recreate_proxy(
    ctx: &async_graphql::Context<'_>,
    ns: String,
    domain: String,
    routes: Vec<route::Data>,
) -> Result<()> {
    let mut proxies_client =
        proxies_client(ctx).map_err(|_| "Failed to create https client".to_string())?;
    proxies_client
        .add_proxy(Request::new(AddProxyRequest {
            domain,
            namespace: ns,
            routes: routes
                .into_iter()
                .map(|r| Route {
                    target_svc: format!("svc-{}", r.service_id),
                    remote_prefix: r.target_path,
                    local_prefix: r.local_path,
                    enable_compression: r.enable_compression,
                    protocol: r.target_protocol.into(),
                    port: r.target_port,
                    https: Some(HttpsOptions {
                        insecure_skip_verify: r.allow_invalid_cert,
                        sni: r.target_sni,
                    }),
                })
                .collect(),
        }))
        .await
        .map_err(|_| "Failed to create proxy in Kubernetes".to_string())?;
    Ok(())
}

#[Object(name = "ProxiesMutations")]
impl Proxies {
    #[graphql(guard = "PermissionGuard::new(Permission::ManageProxies)")]
    async fn create_route(
        &self,
        ctx: &async_graphql::Context<'_>,
        proxy_domain: String,
        local_path: String,
        target_path: String,
        protocol: Protocol,
        target_port: u16,
        enable_compression: bool,
        service_id: String,
        allow_invalid_cert: bool,
        target_sni: Option<String>,
    ) -> Result<String> {
        let db = ctx.data_unchecked::<prisma::PrismaClient>();
        let username = ctx.data_unchecked::<UserCtx>().username.clone();
        let service = db
            .service()
            .find_unique(service::UniqueWhereParam::IdEquals(service_id.clone()))
            .exec()
            .await
            .map_err(|_| "Failed to find service in database".to_string())?
            .ok_or("Failed to find service in database".to_string())?;
        if service.owner_name != username {
            return Err("Failed to find service in database".to_string().into());
        }
        let route = db
            .route()
            .create(
                proxy::UniqueWhereParam::DomainEquals(proxy_domain),
                enable_compression,
                local_path.clone(),
                target_path.clone(),
                service::UniqueWhereParam::IdEquals(service_id.clone()),
                target_port as i32,
                protocol.into(),
                allow_invalid_cert,
                vec![route::SetParam::SetTargetSni(target_sni)],
            )
            .select(route::select!({
                proxy: select {
                    domain
                }
                service_id
                id
            }))
            .exec()
            .await
            .map_err(|_| "Failed to create route in database".to_string())?;
        let all_service_routes = db
            .route()
            .find_many(vec![route::service_id::equals(route.service_id.clone())])
            .exec()
            .await
            .map_err(|_| "Failed to find routes in database".to_string())?;
        recreate_proxy(
            ctx,
            service.owner_name,
            route.proxy.domain,
            all_service_routes,
        )
        .await
        .map_err(|_| "Failed to recreate proxy in Kubernetes".to_string())?;
        Ok(route.id)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageProxies)")]
    async fn delete_route(
        &self,
        ctx: &async_graphql::Context<'_>,
        route_id: String,
    ) -> Result<bool> {
        let db = ctx.data_unchecked::<prisma::PrismaClient>();
        let username = ctx.data_unchecked::<UserCtx>().username.clone();
        let route = db
            .route()
            .find_unique(route::UniqueWhereParam::IdEquals(route_id.clone()))
            .select(route::select!({
                target_service: select {
                    owner_name
                }
                service_id
                proxy: select {
                    domain
                }
            }))
            .exec()
            .await
            .map_err(|_| "Failed to find route in database".to_string())?
            .ok_or("Failed to find route in database".to_string())?;
        let service = db
            .service()
            .find_unique(service::UniqueWhereParam::IdEquals(
                route.service_id.clone(),
            ))
            .exec()
            .await
            .map_err(|_| "Failed to find service in database".to_string())?
            .ok_or("Failed to find service in database".to_string())?;
        if service.owner_name != username {
            return Err("Failed to find service in database".to_string().into());
        }
        db.route()
            .delete(route::UniqueWhereParam::IdEquals(route_id.clone()))
            .exec()
            .await
            .map_err(|_| "Failed to delete route in database".to_string())?;
        let all_service_routes = db
            .route()
            .find_many(vec![route::service_id::equals(route.service_id.clone())])
            .exec()
            .await
            .map_err(|_| "Failed to find routes in database".to_string())?;
        recreate_proxy(
            ctx,
            service.owner_name,
            route.proxy.domain,
            all_service_routes,
        )
        .await
        .map_err(|_| "Failed to recreate proxy in Kubernetes".to_string())?;
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageProxies)")]
    async fn create_proxy(
        &self,
        ctx: &async_graphql::Context<'_>,
        domain: String,
        acme_provider: AcmeProvider,
    ) -> Result<bool> {
        let db = ctx.data_unchecked::<prisma::PrismaClient>();
        let username = ctx.data_unchecked::<UserCtx>().username.clone();
        let _ = db
            .acme_account()
            .find_unique(acme_account::UniqueWhereParam::ProviderOwnerNameEquals(
                acme_provider.into(),
                username.clone(),
            ))
            .select(acme_account::select!({ id }))
            .exec()
            .await
            .map_err(|_| "Failed to find ACME account for this provider, is it enabled?")?
            .ok_or::<Error>(
                "Failed to find ACME account for this provider, is it enabled?".into(),
            )?;
        let sld = domain.split('.').rev().take(2).collect::<Vec<_>>();
        let sld = sld.into_iter().rev().collect::<Vec<_>>().join(".");
        let mut potential_parents = db
            .domain()
            .find_many(vec![
                prisma::domain::owner_name::equals(username.clone()),
                prisma::domain::name::ends_with(sld),
            ])
            .select(prisma::domain::select!({
                id
                name
            }))
            .exec()
            .await
            .map_err::<Error, _>(|_| "Failed to get potential parent domains from DB".into())?;
        potential_parents.sort_by(|potential_parent_a, potential_parent_b| {
            potential_parent_a
                .name
                .len()
                .cmp(&potential_parent_b.name.len())
        });
        db.proxy()
            .create(
                domain.clone(),
                user::UniqueWhereParam::UsernameEquals(username.clone()),
                vec![proxy::SetParam::ConnectAcme(
                    acme_account::UniqueWhereParam::ProviderOwnerNameEquals(
                        acme_provider.into(),
                        username.clone(),
                    ),
                )],
            )
            .exec()
            .await
            .map_err(|_| "Failed to create proxy in database".to_string())?;
        let mut https_client =
            https_client(ctx).map_err(|_| "Failed to create https client".to_string())?;
        https_client
            .add_certificate(Request::new(AddCertificateRequest {
                domain,
                namespace: username.clone(),
                user: username,
                acme_provider: Some(acme_provider.into()),
                parent: potential_parents.last().map(|p| p.name.clone()),
            }))
            .await
            .map_err(|_| "Failed to create proxy in Kubernetes".to_string())?;
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageProxies)")]
    async fn delete_proxy(&self, ctx: &async_graphql::Context<'_>, domain: String) -> Result<bool> {
        let db = ctx.data_unchecked::<crate::prisma::PrismaClient>();
        let username = ctx.data_unchecked::<UserCtx>().username.clone();
        let proxy = db
            .proxy()
            .find_unique(proxy::UniqueWhereParam::DomainEquals(domain.clone()))
            .exec()
            .await
            .map_err(|_| "Failed to find proxy in database".to_string())?
            .ok_or("Failed to find proxy in database".to_string())?;
        if proxy.owner_name != username {
            return Err("Failed to find proxy in database".to_string().into());
        }
        db.proxy()
            .delete(proxy::UniqueWhereParam::DomainEquals(domain.clone()))
            .exec()
            .await
            .map_err(|_| "Failed to delete proxy in database".to_string())?;
        let mut https_client =
            https_client(ctx).map_err(|_| "Failed to create https client".to_string())?;
        https_client
            .remove_certificate(Request::new(DeleteCertificateRequest {
                domain,
                namespace: username.clone(),
            }))
            .await
            .map_err(|_| "Failed to delete certificate in Kubernetes".to_string())?;
        let mut proxies_client =
            proxies_client(ctx).map_err(|_| "Failed to create https client".to_string())?;
        proxies_client
            .remove_proxy(Request::new(RemoveProxyRequest {
                domain: proxy.domain,
                namespace: username.clone(),
            }))
            .await
            .map_err(|_| "Failed to delete route in Kubernetes".to_string())?;
        Ok(true)
    }
}
