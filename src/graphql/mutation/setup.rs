use std::collections::HashMap;

use async_graphql::{Context, Error, Object, Result};
use tonic::Request;

use crate::api::apps::{Empty, InstallRequest, UserState};
use crate::api::core::AddUserAuthRequest;
use crate::graphql::auth_guard::PermissionGuard;
use crate::graphql::clients::{apps_client, middlewares_client, setup_client};
use crate::prisma;
use crate::prisma::{user, Permission, PrismaClient};

#[derive(Default)]
pub struct Setup;

#[Object(name = "SetupMutations")]
impl Setup {
    async fn move_nirvati_to_chart(&self, ctx: &Context<'_>) -> Result<bool> {
        let client = ctx.data::<PrismaClient>()?;
        let total = client
            .user()
            .count(vec![])
            .exec()
            .await
            .map_err(|_| Error::new("Failed to count users"))? as usize;
        if total > 0 {
            return Err(Error::new("Initial user already exists"));
        }
        let mut apps_client = apps_client(ctx)?;
        apps_client
            .apply(Request::new(InstallRequest {
                id: "nirvati".to_string(),
                user_state: Some(UserState {
                    user: "admin".to_string(),
                    installed_apps: vec!["nirvati".to_string(), "chartmuseum".to_string()],
                    app_settings: HashMap::new(),
                }),
                initial_domain: None,
            }))
            .await?;
        apps_client
            .apply(Request::new(InstallRequest {
                id: "chartmuseum".to_string(),
                user_state: Some(UserState {
                    user: "admin".to_string(),
                    installed_apps: vec!["nirvati".to_string(), "chartmuseum".to_string()],
                    app_settings: HashMap::new(),
                }),
                initial_domain: None,
            }))
            .await?;
        Ok(true)
    }

    // The initial user **must** be named admin
    async fn create_initial_user(
        &self,
        ctx: &Context<'_>,
        password: String,
        name: String,
        email: Option<String>,
    ) -> Result<bool> {
        let client = ctx.data::<PrismaClient>()?;
        let total = client
            .user()
            .count(vec![])
            .exec()
            .await
            .map_err(|_| Error::new("Failed to count users"))? as usize;
        if total > 0 {
            return Err(Error::new("Initial user already exists"));
        }
        client
            .user_group()
            .create("main".to_string(), vec![])
            .exec()
            .await
            .map_err(|_| Error::new("Failed to create user group"))?;
        client
            .user()
            .create(
                "admin".to_string(),
                name,
                bcrypt::hash(&password, bcrypt::DEFAULT_COST)
                    .map_err(|_| Error::new("Failed to hash password"))?,
                prisma::user_group::UniqueWhereParam::NameEquals("main".to_string()),
                vec![
                    user::SetParam::SetEmail(email),
                    user::SetParam::SetPermissions(vec![
                        Permission::ManageApps,
                        Permission::ManageDomains,
                        Permission::ManageUsers,
                        Permission::ManageGroups,
                        Permission::ManageProxies,
                        Permission::ManageIssuers,
                        Permission::ManageHardware,
                        Permission::ConfigureNetwork,
                        Permission::ManageSysComponents,
                        Permission::ManageAppStorage,
                        Permission::UseNirvatiMe,
                        Permission::Escalate,
                        Permission::ManageAppStores,
                    ]),
                ],
            )
            .exec()
            .await
            .map_err(|_| Error::new("Failed to create user"))?;

        client
            .app_installation()
            .create(
                "nirvati".to_string(),
                user::UniqueWhereParam::UsernameEquals("admin".to_string()),
                serde_json::Value::Object(Default::default()),
                vec![],
            )
            .exec()
            .await
            .map_err(|_| Error::new("Failed to mark Nirvati as installed"))?;
        client
            .app_installation()
            .create(
                "chartmuseum".to_string(),
                user::UniqueWhereParam::UsernameEquals("admin".to_string()),
                serde_json::Value::Object(Default::default()),
                vec![],
            )
            .exec()
            .await
            .map_err(|_| Error::new("Failed to mark Chartmuseum as installed"))?;

        let mut mw_client = middlewares_client(ctx)?;
        mw_client
            .add_user_auth(Request::new(AddUserAuthRequest {
                user: "admin".to_string(),
                password,
            }))
            .await?;
        Ok(true)
    }

    // Technically not the correct permission, but adding a new perm for this one-time action is overkill
    #[graphql(guard = "PermissionGuard::new(Permission::ManageSysComponents)")]
    pub async fn finish_setup(&self, ctx: &Context<'_>) -> Result<bool> {
        let db_client = ctx.data_unchecked::<PrismaClient>();
        #[cfg(not(feature = "__development"))]
        let mut setup_client = setup_client(ctx)?;
        #[cfg(not(feature = "__development"))]
        setup_client
            .delete_nirvati_ingress(Request::new(Empty {}))
            .await?;
        db_client
            .user()
            .update(
                user::UniqueWhereParam::UsernameEquals("admin".to_string()),
                vec![user::SetParam::SetSetupFinished(true)],
            )
            .exec()
            .await
            .map_err(|_| Error::new("Failed to finish setup"))?;
        Ok(true)
    }
}
