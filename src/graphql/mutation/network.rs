use async_graphql::{Context, Object, Result};

use crate::api::core::Empty;
use crate::graphql::auth_guard::PermissionGuard;
use crate::graphql::clients::tailscale_client;
use crate::prisma::Permission;

#[derive(Default)]
pub struct Network;

#[Object(name = "NetworkMutations")]
impl Network {
    #[graphql(guard = "PermissionGuard::new(Permission::ConfigureNetwork)")]
    async fn start_tailscale_login(&self, ctx: &Context<'_>) -> Result<bool> {
        let mut client = tailscale_client(ctx)?;
        client
            .start_login(tonic::Request::new(Empty {}))
            .await
            .map(|_| true)
            .map_err(|err| err.message().into())
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ConfigureNetwork)")]
    async fn start_tailscale(&self, ctx: &Context<'_>) -> Result<bool> {
        let mut client = tailscale_client(ctx)?;
        client
            .start(tonic::Request::new(Empty {}))
            .await
            .map(|_| true)
            .map_err(|err| err.message().into())
    }
}
