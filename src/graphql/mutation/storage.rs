use async_graphql::{Error, Object};
use k8s_openapi::api::core::v1::PersistentVolumeClaim;
use kube::Api;

use crate::apps::get_app_registry;
use crate::graphql::auth_ctx::UserCtx;
use crate::graphql::auth_guard::PermissionGuard;
use crate::longhorn::Client as LonghornClient;
use crate::prisma;
use crate::prisma::{Permission, PrismaClient};

#[derive(Default)]
pub struct Storage;

#[Object(name = "StorageMutations")]
impl Storage {
    #[graphql(guard = "PermissionGuard::new(Permission::ManageAppStorage)")]
    pub async fn set_replica_count(
        &self,
        ctx: &async_graphql::Context<'_>,
        app_id: String,
        volume_id: String,
        replicas: u8,
    ) -> async_graphql::Result<bool> {
        let user = ctx.data::<UserCtx>()?;
        let prisma_client = ctx.data_unchecked::<PrismaClient>();
        if replicas == 0 {
            return Err(Error::new("Replicas must be greater than 0"));
        }
        let is_installed = prisma_client
            .app_installation()
            .find_unique(
                prisma::app_installation::UniqueWhereParam::AppIdOwnerNameEquals(
                    app_id.clone(),
                    user.username.clone(),
                ),
            )
            .exec()
            .await
            .map(|app| app.is_some())
            .unwrap_or(false);
        if !is_installed {
            return Err(Error::new("App not found"));
        }
        let kube_client = ctx.data_unchecked::<kube::Client>().clone();
        let longhorn_client = ctx.data_unchecked::<LonghornClient>();
        let app_ns = format!("{}-{}", &user.username, app_id);
        let volumes_api: Api<PersistentVolumeClaim> = Api::namespaced(kube_client, &app_ns);
        let volume = volumes_api
            .get(&volume_id)
            .await
            .map_err(|_| Error::new("Failed to get volume"))?;
        let spec = volume.spec.ok_or(Error::new("Failed to get volume spec"))?;
        let longhorn_id = spec
            .volume_name
            .ok_or(Error::new("Failed to get volume name"))?;
        longhorn_client
            .update_replica_count(&longhorn_id, replicas)
            .await?;
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageAppStorage)")]
    pub async fn resize_volume(
        &self,
        ctx: &async_graphql::Context<'_>,
        app_id: String,
        volume_id: String,
        size: u64,
    ) -> async_graphql::Result<bool> {
        let user = ctx.data::<UserCtx>()?;
        let prisma_client = ctx.data_unchecked::<PrismaClient>();
        let is_installed = prisma_client
            .app_installation()
            .find_unique(
                prisma::app_installation::UniqueWhereParam::AppIdOwnerNameEquals(
                    app_id.clone(),
                    user.username.clone(),
                ),
            )
            .exec()
            .await
            .map(|app| app.is_some())
            .unwrap_or(false);
        if !is_installed {
            return Err(Error::new("App not found"));
        }
        let app = get_app_registry(&user.username, &vec![app_id.clone()])
            .map_err(|_| Error::new("Failed to get app registry"))?
            .into_iter()
            .find(|app| app.id == app_id)
            .ok_or(Error::new("App not found"))?;
        let app_volume = app
            .volumes
            .get(&volume_id)
            .ok_or(Error::new("Volume not found"))?;
        if size < app_volume.minimum_size {
            return Err(Error::new("Size too small"));
        };
        let kube_client = ctx.data_unchecked::<kube::Client>().clone();
        let longhorn_client = ctx.data_unchecked::<LonghornClient>();
        let app_ns = format!("{}-{}", &user.username, app_id);
        let volumes_api: Api<PersistentVolumeClaim> = Api::namespaced(kube_client, &app_ns);
        let volume = volumes_api
            .get(&volume_id)
            .await
            .map_err(|_| Error::new("Failed to get volume"))?;
        let spec = volume.spec.ok_or(Error::new("Failed to get volume spec"))?;
        let longhorn_id = spec
            .volume_name
            .ok_or(Error::new("Failed to get volume name"))?;
        let current_vol = longhorn_client
            .get_volume(&longhorn_id)
            .await
            .map_err(|_| Error::new("Failed to get volume"))?;
        let current_size: u64 = current_vol
            .size
            .parse()
            .map_err(|_| Error::new("Failed to parse volume size"))?;
        if current_size >= size {
            return Err(Error::new("Size too small"));
        }
        longhorn_client.expand(&longhorn_id, size).await?;
        Ok(true)
    }
}
