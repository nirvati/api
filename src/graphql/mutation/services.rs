use async_graphql::{Object, Result};
use tonic::Request;

use crate::api::core::add_service_request::Remote;
use crate::api::core::{AddServiceRequest, RemoveServiceRequest};
use crate::graphql::auth_ctx::UserCtx;
use crate::graphql::auth_guard::PermissionGuard;
use crate::graphql::clients::services_client;
use crate::prisma::{service, user, Permission, UpstreamType};

#[derive(Default)]
pub struct Services;

#[Object(name = "ServicesMutations")]
impl Services {
    #[graphql(guard = "PermissionGuard::new(Permission::ManageProxies)")]
    async fn create_service(
        &self,
        ctx: &async_graphql::Context<'_>,
        ip: Option<String>,
        dns_name: Option<String>,
        tcp_ports: Vec<u16>,
        udp_ports: Vec<u16>,
    ) -> Result<String> {
        let db = ctx.data_unchecked::<crate::prisma::PrismaClient>();
        let username = ctx.data_unchecked::<UserCtx>().username.clone();
        let mut services_client =
            services_client(ctx).map_err(|_| "Failed to create services client".to_string())?;
        if let Some(ip) = ip {
            let service = db
                .service()
                .create(
                    ip.clone(),
                    UpstreamType::Ip,
                    user::UniqueWhereParam::UsernameEquals(username.clone()),
                    vec![
                        service::SetParam::SetTcpPorts(
                            tcp_ports.iter().map(|port| *port as i32).collect(),
                        ),
                        service::SetParam::SetUdpPorts(
                            udp_ports.iter().map(|port| *port as i32).collect(),
                        ),
                    ],
                )
                .exec()
                .await
                .map_err(|_| "Failed to create service in database".to_string())?;
            services_client
                .add_service(Request::new(AddServiceRequest {
                    namespace: username,
                    name: format!("svc-{}", service.id),
                    tcp_ports: tcp_ports.into_iter().map(|port| port as u32).collect(),
                    udp_ports: udp_ports.into_iter().map(|port| port as u32).collect(),
                    remote: Some(Remote::Ip(ip)),
                }))
                .await
                .map_err(|e| {
                    tracing::error!("{:#?}", e);
                    "Failed to create service in Kubernetes".to_string()
                })?;
            Ok(service.id)
        } else if let Some(dns_name) = dns_name {
            let service = db
                .service()
                .create(
                    dns_name.clone(),
                    UpstreamType::Dns,
                    user::UniqueWhereParam::UsernameEquals(username.clone()),
                    vec![
                        service::SetParam::SetTcpPorts(
                            tcp_ports.iter().map(|port| *port as i32).collect(),
                        ),
                        service::SetParam::SetUdpPorts(
                            udp_ports.iter().map(|port| *port as i32).collect(),
                        ),
                    ],
                )
                .exec()
                .await
                .map_err(|_| "Failed to create service in database".to_string())?;
            services_client
                .add_service(Request::new(AddServiceRequest {
                    namespace: username,
                    name: format!("svc-{}", service.id),
                    tcp_ports: tcp_ports.into_iter().map(|port| port as u32).collect(),
                    udp_ports: udp_ports.into_iter().map(|port| port as u32).collect(),
                    remote: Some(Remote::DnsName(dns_name)),
                }))
                .await
                .map_err(|e| {
                    tracing::error!("{:#?}", e);
                    "Failed to create service in Kubernetes".to_string()
                })?;
            Ok(service.id)
        } else {
            Err("Either ip or dnsName must be provided".into())
        }
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageProxies)")]
    async fn add_service_ports(
        &self,
        ctx: &async_graphql::Context<'_>,
        id: String,
        tcp_ports: Vec<u16>,
        udp_ports: Vec<u16>,
    ) -> Result<bool> {
        let db = ctx.data_unchecked::<crate::prisma::PrismaClient>();
        let username = ctx.data_unchecked::<UserCtx>().username.clone();
        let mut services_client =
            services_client(ctx).map_err(|_| "Failed to create services client".to_string())?;
        let found_service = db
            .service()
            .find_unique(service::UniqueWhereParam::IdEquals(id.clone()))
            .exec()
            .await
            .map_err(|_| "Failed to find service in database".to_string())?
            .ok_or("Service not found")?;
        if found_service.owner_name != username {
            return Err("Service not found".into());
        }
        let updated = db
            .service()
            .update(
                service::UniqueWhereParam::IdEquals(id.clone()),
                vec![
                    service::SetParam::PushTcpPorts(
                        tcp_ports.iter().map(|port| *port as i32).collect(),
                    ),
                    service::SetParam::PushUdpPorts(
                        udp_ports.iter().map(|port| *port as i32).collect(),
                    ),
                ],
            )
            .exec()
            .await
            .map_err(|_| "Failed to update service in database".to_string())?;
        services_client
            .remove_service(Request::new(RemoveServiceRequest {
                namespace: username.clone(),
                name: format!("svc-{}", id),
            }))
            .await
            .map_err(|_| "Failed to delete service in Kubernetes".to_string())?;
        services_client
            .add_service(Request::new(AddServiceRequest {
                namespace: username,
                name: format!("svc-{}", id),
                tcp_ports: updated
                    .tcp_ports
                    .into_iter()
                    .map(|port| port as u32)
                    .collect(),
                udp_ports: updated
                    .udp_ports
                    .into_iter()
                    .map(|port| port as u32)
                    .collect(),
                remote: Some(Remote::Ip(found_service.upstream.clone())),
            }))
            .await
            .map_err(|e| {
                tracing::error!("{:#?}", e);
                "Failed to create service in Kubernetes".to_string()
            })?;
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageProxies)")]
    async fn delete_service(&self, ctx: &async_graphql::Context<'_>, id: String) -> Result<bool> {
        let db = ctx.data_unchecked::<crate::prisma::PrismaClient>();
        let username = ctx.data_unchecked::<UserCtx>().username.clone();
        let mut services_client =
            services_client(ctx).map_err(|_| "Failed to create services client".to_string())?;
        let found_service = db
            .service()
            .find_unique(service::UniqueWhereParam::IdEquals(id.clone()))
            .exec()
            .await
            .map_err(|_| "Failed to find service in database".to_string())?
            .ok_or("Service not found")?;
        if found_service.owner_name != username {
            return Err("Service not found".into());
        }
        db.service()
            .delete(service::UniqueWhereParam::IdEquals(id.clone()))
            .exec()
            .await
            .map_err(|_| "Failed to delete service in database".to_string())?;
        services_client
            .remove_service(Request::new(RemoveServiceRequest {
                namespace: username,
                name: format!("svc-{}", id),
            }))
            .await
            .map_err(|_| "Failed to delete service in Kubernetes".to_string())?;
        Ok(true)
    }
}
