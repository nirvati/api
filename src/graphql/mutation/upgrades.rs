use async_graphql::{Context, Object, Result};

use crate::api::core::VersionInfo;
use crate::graphql::auth_guard::PermissionGuard;
use crate::graphql::clients::upgrades_client;
use crate::prisma::Permission;

#[derive(Default)]
pub struct Upgrades;

#[Object(name = "UpgradeMutations")]
impl Upgrades {
    #[graphql(guard = "PermissionGuard::new(Permission::ManageSysComponents)")]
    async fn upgrade_k3s(&self, ctx: &Context<'_>, target_version: String) -> Result<bool> {
        let mut client = upgrades_client(ctx)?;
        client
            .update_k3s(tonic::Request::new(VersionInfo {
                version: target_version,
            }))
            .await
            .map(|_| true)
            .map_err(|err| err.to_string().into())
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageSysComponents)")]
    async fn upgrade_cert_manager(
        &self,
        ctx: &Context<'_>,
        target_version: String,
    ) -> Result<bool> {
        let mut client = upgrades_client(ctx)?;
        client
            .update_cert_manager(tonic::Request::new(VersionInfo {
                version: target_version,
            }))
            .await
            .map(|_| true)
            .map_err(|err| err.to_string().into())
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageSysComponents)")]
    async fn upgrade_longhorn(&self, ctx: &Context<'_>, target_version: String) -> Result<bool> {
        let mut client = upgrades_client(ctx)?;
        client
            .update_longhorn(tonic::Request::new(VersionInfo {
                version: target_version,
            }))
            .await
            .map(|_| true)
            .map_err(|err| err.to_string().into())
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageSysComponents)")]
    async fn upgrade_system_upgrade_controller(
        &self,
        ctx: &Context<'_>,
        target_version: String,
    ) -> Result<bool> {
        let mut client = upgrades_client(ctx)?;
        client
            .update_system_upgrade_controller(tonic::Request::new(VersionInfo {
                version: target_version,
            }))
            .await
            .map(|_| true)
            .map_err(|err| err.to_string().into())
    }
}
