use async_graphql::{Error, Result};

use crate::prisma::{user, Permission};
use crate::utils::permissions::PermissionSet;

pub struct UserCtx {
    pub username: String,
    pub permissions: PermissionSet,
    pub user_group: String,
    // Include this because it's needed in a few places, and we don't want to always get it from the DB
    pub email: Option<String>,
}

impl UserCtx {
    pub fn get_user_restrictions(
        &self,
        requested_group: Option<String>,
    ) -> Result<Option<user::WhereParam>> {
        Ok(if self.permissions.has(Permission::ManageGroups) {
            requested_group.map(user::user_group_id::equals)
        } else if let Some(user_group) = requested_group {
            if user_group != self.user_group {
                return Err(Error::new("Forbidden"));
            };
            Some(user::user_group_id::equals(user_group))
        } else {
            Some(user::user_group_id::equals(self.user_group.clone()))
        })
    }
}
