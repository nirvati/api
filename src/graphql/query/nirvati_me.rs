use anyhow::Result;
use async_graphql::SimpleObject;
use serde::Deserialize;

#[derive(SimpleObject, Default, Deserialize)]
pub struct NirvatiMeDetails {
    pub created_at: String,
    pub ip: String,
    pub last_ping: String,
    pub name: String,
}

pub async fn get_details(username: &str, password: &str) -> Result<NirvatiMeDetails> {
    let client = reqwest::Client::new();
    let response = client
        .get("https://nirvati.me/api/dns/getInfo")
        .basic_auth(username, Some(password))
        .send()
        .await?;
    let details = response.json::<NirvatiMeDetails>().await?;
    Ok(details)
}
