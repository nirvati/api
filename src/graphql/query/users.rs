#![allow(non_camel_case_types)]

use async_graphql::connection::{query, Connection, Edge};
use async_graphql::{Context, Error, Object, Result};

pub use permission::Permission;

use crate::graphql::auth_ctx::UserCtx;
use crate::graphql::auth_guard::PermissionGuard;
use crate::graphql::query::users::user::user_metadata;
pub use crate::graphql::query::users::user::User;
use crate::prisma;
use crate::prisma::user::WhereParam;
use crate::prisma::{Permission as PrismaPermission, PrismaClient};

mod permission;
pub mod user;

pub struct Users;

#[Object]
impl Users {
    async fn me(&self, ctx: &Context<'_>) -> Result<User> {
        Ok(User::from_username(ctx.data::<UserCtx>()?.username.clone()))
    }

    async fn user(&self, ctx: &Context<'_>, username: String) -> Result<User> {
        let user_ctx = ctx.data::<UserCtx>()?;
        let user_restriction = user_ctx.get_user_restrictions(None)?;
        let mut find_params = vec![prisma::user::username::equals(username)];
        if let Some(user_restriction) = user_restriction {
            find_params.push(user_restriction);
        }
        let user = ctx
            .data::<PrismaClient>()?
            .user()
            .find_first(find_params)
            .select(user_metadata::select())
            .exec()
            .await
            .map_err(|_| Error::new("User not found"))?
            .ok_or(Error::new("User not found"))?;
        Ok(user.into())
    }

    #[graphql(guard = "PermissionGuard::new(PrismaPermission::ManageUsers)")]
    async fn users(
        &self,
        ctx: &Context<'_>,
        search_name_id: Option<String>,
        user_group: Option<String>,
        email: Option<String>,
        after: Option<String>,
        before: Option<String>,
        first: Option<i32>,
        last: Option<i32>,
    ) -> Result<Connection<usize, User>> {
        let user_ctx = ctx.data::<UserCtx>()?;
        let user_restriction = user_ctx.get_user_restrictions(user_group)?;
        query(
            after,
            before,
            first,
            last,
            |after, before, first, last| async move {
                let mut start = 0usize;
                let mut filters = Vec::new();
                if let Some(user_restriction) = user_restriction {
                    filters.push(user_restriction);
                }
                if let Some(ref search_name) = search_name_id {
                    filters.push(WhereParam::Or(vec![
                        prisma::user::username::search(search_name.clone()),
                        prisma::user::name::search(search_name.clone()),
                    ]));
                }
                if let Some(ref email) = email {
                    filters.push(prisma::user::email::search(email.clone()));
                }
                let total = ctx
                    .data::<PrismaClient>()?
                    .user()
                    .count(filters.clone())
                    .exec()
                    .await
                    .map_err(|_| Error::new("Failed to count users"))?
                    as usize;
                let mut end = total;
                if let Some(after) = after {
                    if after >= total {
                        let has_previous_page = (after - start) > 0;
                        return Ok(Connection::new(has_previous_page, false));
                    }
                    start = after + 1;
                }
                if let Some(before) = before {
                    if before == 0 {
                        return Ok(Connection::new(false, false));
                    }
                    end = before;
                }
                if let Some(first) = first {
                    let new_end = start + first;
                    if new_end < end {
                        end = new_end;
                    }
                }
                if let Some(last) = last {
                    let new_start = end - last;
                    if new_start > start {
                        start = new_start;
                    }
                }

                let mut i = start;
                ctx.data::<PrismaClient>()?
                    .user()
                    .find_many(filters)
                    .skip(start as i64)
                    .take((end - start) as i64)
                    .select(user_metadata::select())
                    .exec()
                    .await
                    .map(|users| {
                        let mut edges = vec![];
                        for user in users {
                            edges.push(Edge::new(i, user.into()));
                            i += 1;
                        }
                        let mut connection = Connection::new(start > 0, end < total);
                        connection.edges.append(&mut edges);
                        connection
                    })
                    .map_err(|_| Error::new("Failed to find users"))
            },
        )
        .await
    }
}
