use async_graphql::{ComplexObject, Context, Error, Result, SimpleObject};
use tonic::Request;

use crate::api::https::GetCertificateStatusRequest;
use crate::graphql::clients::https_client;
use crate::graphql::query::https::user_domain::UserDomain;
use crate::graphql::query::users::User;
use crate::prisma::domain;

#[derive(SimpleObject)]
#[graphql(complex)]
pub struct AppDomain {
    pub name: String,
    pub created_at: chrono::DateTime<chrono::Utc>,
    // TODO: Reference to the app
    // An app domain can currently also refer to Nirvati itself, so it's not possible yet
    pub app_id: String,
    #[graphql(skip)]
    pub owner_name: String,
    #[graphql(skip)]
    pub parent: Option<String>,
}

#[ComplexObject]
impl AppDomain {
    async fn parent_domain(&self, ctx: &Context<'_>) -> Result<Option<UserDomain>> {
        let parent_id = self.parent.clone();
        let Some(parent) = parent_id else {
            return Ok(None);
        };
        let domain = ctx
            .data::<crate::prisma::PrismaClient>()?
            .domain()
            .find_unique(domain::UniqueWhereParam::NameOwnerNameEquals(
                parent,
                self.owner_name.clone(),
            ))
            .exec()
            .await
            .map_err::<Error, _>(|err| err.to_string().into())?
            // Should be basically impossible to happen
            .ok_or::<Error>("Domain not found".into())?;
        Ok(Some(UserDomain {
            created_at: domain.created_at.into(),
            domain: domain.name,
            dns_provider: domain.provider.into(),
            dns_provider_auth: domain.provider_auth,
            owner_name: domain.owner_name,
        }))
    }
    async fn user(&self) -> Result<User> {
        let user_id = self.owner_name.clone();
        Ok(User::from_username(user_id))
    }

    async fn ready(&self, ctx: &Context<'_>) -> Result<bool> {
        let mut https_client = https_client(ctx)?;
        https_client
            .get_certificate_status(Request::new(GetCertificateStatusRequest {
                namespace: format!("{}-{}", self.owner_name, self.app_id),
                domain: self.name.clone(),
            }))
            .await
            .map_err::<Error, _>(|err| err.to_string().into())
            .map(|response| response.into_inner().ready)
    }
}
