use async_graphql::{ComplexObject, Context, Enum, Error, SimpleObject};

use crate::graphql::query::users::user::user_metadata;
use crate::graphql::query::users::User;
use crate::prisma::read_filters::StringFilter;
use crate::prisma::{user, PrismaClient};

#[derive(Enum, Copy, Clone, PartialEq, Eq, Debug)]
#[graphql(remote = "crate::prisma::Protocol")]
pub enum Protocol {
    Http,
    Https,
}

#[derive(Enum, Copy, Clone, PartialEq, Eq, Debug)]
#[graphql(remote = "crate::prisma::UpstreamType")]
pub enum UpstreamType {
    Dns,
    Ip,
}

#[derive(SimpleObject)]
#[graphql(complex)]
pub struct Proxy {
    pub domain: String,
    #[graphql(skip)]
    pub owner: String,
}

#[derive(SimpleObject)]
#[graphql(complex)]
pub struct TcpProxy {
    pub domain: String,
    #[graphql(skip)]
    pub owner: String,
    #[graphql(skip)]
    pub service_id: String,
    pub target_port: u16,
}

#[derive(SimpleObject)]
#[graphql(complex)]
pub struct Route {
    pub id: String,
    pub enable_compression: bool,
    pub local_path: String,
    pub target_path: String,
    #[graphql(skip)]
    pub service_id: String,
    #[graphql(skip)]
    pub owner: String,
    pub protocol: Protocol,
    pub target_port: u16,
}

#[derive(SimpleObject)]
#[graphql(complex)]
pub struct Service {
    pub id: String,
    pub upstream: String,
    pub upstream_type: UpstreamType,
    #[graphql(skip)]
    pub owner: String,
    pub tcp_ports: Vec<u16>,
    pub udp_ports: Vec<u16>,
}

#[ComplexObject]
impl Proxy {
    async fn owner(&self, ctx: &Context<'_>) -> async_graphql::Result<User> {
        let user_id = self.owner.clone();
        let user = ctx
            .data::<PrismaClient>()?
            .user()
            .find_unique(user::username::equals(user_id))
            .select(user_metadata::select())
            .exec()
            .await
            .map_err::<Error, _>(|err| err.to_string().into())?
            // Should be basically impossible to happen
            .ok_or::<Error>("User not found".into())?;
        Ok(user.into())
    }

    async fn routes(&self, ctx: &Context<'_>) -> async_graphql::Result<Vec<Route>> {
        let routes = ctx
            .data::<PrismaClient>()?
            .route()
            .find_many(vec![crate::prisma::route::WhereParam::ProxyIs(vec![
                crate::prisma::proxy::WhereParam::Domain(StringFilter::Equals(self.domain.clone())),
            ])])
            .exec()
            .await
            .map_err::<Error, _>(|err| err.to_string().into())?;
        Ok(routes
            .into_iter()
            .map(|route| Route {
                enable_compression: route.enable_compression,
                local_path: route.local_path,
                target_path: route.target_path,
                service_id: route.service_id,
                owner: self.owner.clone(),
                protocol: route.target_protocol.into(),
                target_port: route.target_port as u16,
                id: route.id,
            })
            .collect())
    }
}

#[ComplexObject]
impl Route {
    async fn service(&self, ctx: &Context<'_>) -> async_graphql::Result<Service> {
        let service = ctx
            .data::<PrismaClient>()?
            .service()
            .find_unique(crate::prisma::service::UniqueWhereParam::IdEquals(
                self.service_id.clone(),
            ))
            .exec()
            .await
            .map_err::<Error, _>(|err| err.to_string().into())?
            // Should be basically impossible to happen
            .ok_or::<Error>("Service not found".into())?;
        Ok(Service {
            id: service.id,
            owner: service.owner_name,
            upstream: service.upstream,
            upstream_type: service.r#type.into(),
            tcp_ports: service.tcp_ports.into_iter().map(|p| p as u16).collect(),
            udp_ports: service.udp_ports.into_iter().map(|p| p as u16).collect(),
        })
    }

    async fn owner(&self, ctx: &Context<'_>) -> async_graphql::Result<User> {
        let user_id = self.owner.clone();
        let user = ctx
            .data::<PrismaClient>()?
            .user()
            .find_unique(user::username::equals(user_id))
            .select(user_metadata::select())
            .exec()
            .await
            .map_err::<Error, _>(|err| err.to_string().into())?
            // Should be basically impossible to happen
            .ok_or::<Error>("User not found".into())?;
        Ok(user.into())
    }
}

#[ComplexObject]
impl Service {
    async fn owner(&self, ctx: &Context<'_>) -> async_graphql::Result<User> {
        let user_id = self.owner.clone();
        let user = ctx
            .data::<PrismaClient>()?
            .user()
            .find_unique(user::username::equals(user_id))
            .select(user_metadata::select())
            .exec()
            .await
            .map_err::<Error, _>(|err| err.to_string().into())?
            // Should be basically impossible to happen
            .ok_or::<Error>("User not found".into())?;
        Ok(user.into())
    }
}

#[ComplexObject]
impl TcpProxy {
    async fn owner(&self, ctx: &Context<'_>) -> async_graphql::Result<User> {
        let user_id = self.owner.clone();
        let user = ctx
            .data::<PrismaClient>()?
            .user()
            .find_unique(user::username::equals(user_id))
            .select(user_metadata::select())
            .exec()
            .await
            .map_err::<Error, _>(|err| err.to_string().into())?
            // Should be basically impossible to happen
            .ok_or::<Error>("User not found".into())?;
        Ok(user.into())
    }

    async fn service(&self, ctx: &Context<'_>) -> async_graphql::Result<Service> {
        let service = ctx
            .data::<PrismaClient>()?
            .service()
            .find_unique(crate::prisma::service::UniqueWhereParam::IdEquals(
                self.service_id.clone(),
            ))
            .exec()
            .await
            .map_err::<Error, _>(|err| err.to_string().into())?
            // Should be basically impossible to happen
            .ok_or::<Error>("Service not found".into())?;
        Ok(Service {
            id: service.id,
            owner: service.owner_name,
            upstream: service.upstream,
            upstream_type: service.r#type.into(),
            tcp_ports: service.tcp_ports.into_iter().map(|p| p as u16).collect(),
            udp_ports: service.udp_ports.into_iter().map(|p| p as u16).collect(),
        })
    }
}
