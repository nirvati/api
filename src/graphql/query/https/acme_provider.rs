use async_graphql::Enum;

use crate::api::https::acme_provider::Provider as GrpcAcmeProvider;
use crate::api::https::AcmeProvider as GrpcAcmeProviderWrapper;
use crate::api::https::BuiltInAcmeProvider as GrpcInnerAcmeProvider;
use crate::prisma::AcmeProvider as DbAcmeProvider;

#[derive(Enum, Clone, PartialOrd, PartialEq, Eq, Copy, Debug)]
#[graphql(remote = "DbAcmeProvider")]
pub enum AcmeProvider {
    LetsEncrypt,
    BuyPass,
}

impl From<AcmeProvider> for GrpcAcmeProvider {
    fn from(val: AcmeProvider) -> Self {
        match val {
            AcmeProvider::LetsEncrypt => {
                GrpcAcmeProvider::BuiltIn(GrpcInnerAcmeProvider::LetsEncrypt.into())
            }
            AcmeProvider::BuyPass => {
                GrpcAcmeProvider::BuiltIn(GrpcInnerAcmeProvider::BuyPass.into())
            }
        }
    }
}

impl From<AcmeProvider> for GrpcAcmeProviderWrapper {
    fn from(val: AcmeProvider) -> Self {
        GrpcAcmeProviderWrapper {
            provider: Some(val.into()),
        }
    }
}

impl From<GrpcInnerAcmeProvider> for AcmeProvider {
    fn from(provider: GrpcInnerAcmeProvider) -> Self {
        match provider {
            GrpcInnerAcmeProvider::LetsEncrypt => AcmeProvider::LetsEncrypt,
            GrpcInnerAcmeProvider::BuyPass => AcmeProvider::BuyPass,
        }
    }
}
