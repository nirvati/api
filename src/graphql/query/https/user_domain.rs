use async_graphql::connection::{query, Connection, Edge};
use async_graphql::{ComplexObject, Context, Enum, Error, Result, SimpleObject};

use crate::graphql::query::https::app_domain::AppDomain;
use crate::graphql::query::users::user::user_metadata;
use crate::graphql::query::users::User;
use crate::prisma;
use crate::prisma::{app_domain, user, PrismaClient};

#[derive(Enum, PartialEq, Eq, Copy, Clone, Debug)]
#[graphql(remote = "crate::prisma::DnsProvider")]
pub enum DnsProvider {
    Cloudflare,
    NirvatiMe,
    None,
}

impl From<crate::api::https::DnsProvider> for DnsProvider {
    fn from(provider: crate::api::https::DnsProvider) -> Self {
        match provider {
            crate::api::https::DnsProvider::Cloudflare => DnsProvider::Cloudflare,
            crate::api::https::DnsProvider::NirvatiMe => DnsProvider::NirvatiMe,
            crate::api::https::DnsProvider::None => DnsProvider::None,
        }
    }
}

impl From<DnsProvider> for crate::api::https::DnsProvider {
    fn from(provider: DnsProvider) -> Self {
        match provider {
            DnsProvider::Cloudflare => crate::api::https::DnsProvider::Cloudflare,
            DnsProvider::NirvatiMe => crate::api::https::DnsProvider::NirvatiMe,
            DnsProvider::None => crate::api::https::DnsProvider::None,
        }
    }
}

#[derive(SimpleObject)]
#[graphql(complex)]
pub struct UserDomain {
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub domain: String,
    pub dns_provider: DnsProvider,
    pub dns_provider_auth: Option<String>,
    #[graphql(skip)]
    pub owner_name: String,
}

#[ComplexObject]
impl UserDomain {
    async fn user(&self, ctx: &Context<'_>) -> Result<User> {
        let user_id = self.owner_name.clone();
        let user = ctx
            .data::<PrismaClient>()?
            .user()
            .find_unique(user::username::equals(user_id))
            .select(user_metadata::select())
            .exec()
            .await
            .map_err::<Error, _>(|err| err.to_string().into())?
            // Should be basically impossible to happen
            .ok_or::<Error>("User not found".into())?;
        Ok(user.into())
    }

    async fn app_domains(
        &self,
        ctx: &Context<'_>,
        after: Option<String>,
        before: Option<String>,
        first: Option<i32>,
        last: Option<i32>,
    ) -> Result<Connection<usize, AppDomain>> {
        query(
            after,
            before,
            first,
            last,
            |after, before, first, last| async move {
                let mut start = 0usize;
                let total = ctx
                    .data::<PrismaClient>()?
                    .app_domain()
                    .count(vec![app_domain::parent_domain_name::equals(Some(
                        self.domain.clone(),
                    ))])
                    .exec()
                    .await
                    .map_err(|_| Error::new("Failed to count users"))?
                    as usize;
                let mut end = total;
                if let Some(after) = after {
                    if after >= total {
                        let has_previous_page = (after - start) > 0;
                        return Ok(Connection::new(has_previous_page, false));
                    }
                    start = after + 1;
                }
                if let Some(before) = before {
                    if before == 0 {
                        return Ok(Connection::new(false, false));
                    }
                    end = before;
                }
                if let Some(first) = first {
                    let new_end = start + first;
                    if new_end < end {
                        end = new_end;
                    }
                }
                if let Some(last) = last {
                    let new_start = end - last;
                    if new_start > start {
                        start = new_start;
                    }
                }

                ctx.data::<PrismaClient>()?
                    .app_domain()
                    .find_many(vec![app_domain::parent_domain_name::equals(Some(
                        self.domain.clone(),
                    ))])
                    .skip(start as i64)
                    .take((end - start) as i64)
                    .select(prisma::app_domain::select!({
                        name
                        created_at
                        parent_domain_name
                        app_installation: select {
                            owner_name
                            app_id
                        }
                    }))
                    .exec()
                    .await
                    .map(|app_domains| {
                        let mut edges = vec![];
                        for app_domain in app_domains {
                            edges.push(Edge::new(
                                start,
                                AppDomain {
                                    name: app_domain.name,
                                    created_at: app_domain.created_at.into(),
                                    app_id: app_domain.app_installation.app_id.clone(),
                                    owner_name: app_domain.app_installation.owner_name.clone(),
                                    parent: app_domain.parent_domain_name,
                                },
                            ));
                        }
                        let mut connection = Connection::new(start > 0, end < total);
                        connection.edges.append(&mut edges);
                        connection
                    })
                    .map_err(|_| Error::new("Failed to find users"))
            },
        )
        .await
    }
}
