use std::collections::BTreeMap;

use async_graphql::{InputValueError, InputValueResult, Name, Scalar, ScalarType, Value};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq, Default)]
pub struct MultiLangItem(pub(crate) BTreeMap<String, String>);

#[Scalar]
impl ScalarType for MultiLangItem {
    fn parse(value: Value) -> InputValueResult<Self> {
        if let Value::String(value) = &value {
            // Parse the integer value
            Ok(MultiLangItem(BTreeMap::from([(
                "en".to_string(),
                value.clone(),
            )])))
        } else if let Value::Object(value) = &value {
            let mut map = BTreeMap::new();
            for (k, v) in value {
                if let Value::String(v) = v {
                    map.insert(k.as_str().to_string(), v.clone());
                } else {
                    return Err(InputValueError::expected_type(v.to_owned()));
                }
            }
            Ok(MultiLangItem(map))
        } else {
            Err(InputValueError::expected_type(value))
        }
    }

    fn to_value(&self) -> Value {
        Value::Object(
            self.0
                .clone()
                .into_iter()
                .map(|(k, v)| (Name::new(k), Value::String(v)))
                .collect(),
        )
    }
}
