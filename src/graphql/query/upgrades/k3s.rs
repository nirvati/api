use async_graphql::{Context, Error, Object, Result};

use crate::api::core::Empty;
use crate::graphql::clients::upgrades_client;

pub struct K3sInfo;

#[Object]
impl K3sInfo {
    async fn installed_version(&self, ctx: &Context<'_>) -> Result<Option<String>> {
        let mut client = upgrades_client(ctx)?;
        client
            .get_k3s_version(tonic::Request::new(Empty {}))
            .await
            .map(|data| data.into_inner().version)
            .map_err(|err| err.to_string().into())
    }

    /// Returns the latest version of K3s that we can safely upgrade to, taking Kubernetes version skew into account.
    async fn latest_version(&self, ctx: &Context<'_>) -> Result<Option<String>> {
        let mut client = upgrades_client(ctx)?;
        let current_version = client
            .get_k3s_version(tonic::Request::new(Empty {}))
            .await
            .map(|data| data.into_inner().version)
            .map_err::<Error, _>(|err| err.to_string().into())
            .is_ok_and(|data| data.is_some());
        if !current_version {
            Ok(None)
        } else {
            client
                .get_latest_k3s_version(tonic::Request::new(Empty {}))
                .await
                .map(|data| Some(data.into_inner().version))
                .map_err::<Error, _>(|err| err.to_string().into())
        }
    }
}
