use async_graphql::{Context, Error, Object, Result};

use crate::api::core::Empty;
use crate::graphql::clients::upgrades_client;

pub struct CertManagerInfo;

#[Object]
impl CertManagerInfo {
    async fn installed_version(&self, ctx: &Context<'_>) -> Result<String> {
        let mut client = upgrades_client(ctx)?;
        client
            .get_cert_manager_version(tonic::Request::new(Empty {}))
            .await
            .map(|data| data.into_inner().version)
            .map_err(|err| err.to_string().into())
    }

    /// Returns the latest version of cert-manager that we can safely upgrade to (semver-wise).
    async fn latest_version(&self, ctx: &Context<'_>) -> Result<String> {
        let mut client = upgrades_client(ctx)?;
        client
            .get_latest_cert_manager_version(tonic::Request::new(Empty {}))
            .await
            .map(|data| data.into_inner().version)
            .map_err::<Error, _>(|err| err.to_string().into())
    }
}
