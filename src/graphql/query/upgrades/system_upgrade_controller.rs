use async_graphql::{Context, Error, Object, Result};

use crate::api::core::Empty;
use crate::graphql::clients::upgrades_client;

pub struct SystemUpgradeControllerInfo;

#[Object]
impl SystemUpgradeControllerInfo {
    async fn installed_version(&self, ctx: &Context<'_>) -> Result<String> {
        let mut client = upgrades_client(ctx)?;
        client
            .get_system_upgrade_controller_version(tonic::Request::new(Empty {}))
            .await
            .map(|data| data.into_inner().version)
            .map_err(|err| err.to_string().into())
    }

    /// Returns the latest version of system-upgrade-controller that we can safely upgrade to (semver-wise).
    async fn latest_version(&self, ctx: &Context<'_>) -> Result<String> {
        let mut client = upgrades_client(ctx)?;
        client
            .get_latest_system_upgrade_controller_version(tonic::Request::new(Empty {}))
            .await
            .map(|data| data.into_inner().version)
            .map_err::<Error, _>(|err| err.to_string().into())
    }
}
