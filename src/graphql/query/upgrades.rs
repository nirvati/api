use async_graphql::Object;

mod cert_manager;
mod k3s;
mod longhorn;
mod system_upgrade_controller;

pub struct Upgrades;

#[Object]
impl Upgrades {
    async fn k3s(&self) -> k3s::K3sInfo {
        k3s::K3sInfo
    }

    async fn cert_manager(&self) -> cert_manager::CertManagerInfo {
        cert_manager::CertManagerInfo
    }

    async fn longhorn(&self) -> longhorn::LonghornInfo {
        longhorn::LonghornInfo
    }

    async fn system_upgrade_controller(&self) -> system_upgrade_controller::SystemUpgradeControllerInfo {
        system_upgrade_controller::SystemUpgradeControllerInfo
    }
}
