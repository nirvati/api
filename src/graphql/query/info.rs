use async_graphql::{Context, Error, Object, Result};

use crate::instance::{InstanceType, INSTANCE_TYPE};

#[derive(Default)]
pub struct Info;

#[Object]
impl Info {
    pub async fn is_user_setup(&self, ctx: &Context<'_>) -> Result<bool> {
        let db = ctx.data::<crate::prisma::PrismaClient>().unwrap();
        let count = db
            .user()
            .count(vec![])
            .exec()
            .await
            .map_err::<Error, _>(|err| err.to_string().into())?;
        Ok(count > 0)
    }

    pub async fn is_setup_finished(&self, ctx: &Context<'_>) -> Result<bool> {
        ctx.data::<crate::prisma::PrismaClient>()
            .unwrap()
            .user()
            .find_unique(crate::prisma::user::UniqueWhereParam::UsernameEquals(
                "admin".to_string(),
            ))
            .select(crate::prisma::user::select!({ setup_finished }))
            .exec()
            .await
            .map_err::<Error, _>(|err| err.to_string().into())
            .map(|user| user.map(|user| user.setup_finished).unwrap_or(false))
    }

    pub async fn instance_type(&self) -> InstanceType {
        *INSTANCE_TYPE.get().unwrap()
    }
}
