use async_graphql::{Context, Error, Object, Result, SimpleObject};

use crate::api::core::Empty;
use crate::graphql::auth_guard::PermissionGuard;
use crate::graphql::clients::{node_client, tailscale_client};
use crate::prisma::Permission;

#[derive(SimpleObject, Default)]
pub struct TailscaleInfo {
    pub ip: Option<String>,
    pub auth_url: Option<String>,
}

pub struct Network;

#[Object]
impl Network {
    async fn ip(&self, ctx: &Context<'_>) -> Result<String> {
        let mut client = node_client(ctx)?;
        client
            .get_info(tonic::Request::new(Empty {}))
            .await
            .map(|data| data.into_inner().ip)
            .map_err(|err| err.message().into())
    }

    // Guarded again (normally covered by the Query guard) in case we expose IP without permission in the future
    #[graphql(guard = "PermissionGuard::new(Permission::ConfigureNetwork)")]
    async fn tailscale_info(&self, ctx: &Context<'_>) -> Result<TailscaleInfo> {
        let mut client = tailscale_client(ctx)?;
        let resp = client
            .get_status(tonic::Request::new(Empty {}))
            .await
            .map(|data| data.into_inner())
            .map_err::<Error, _>(|err| err.message().into())?;
        if resp.authenticated {
            Ok(TailscaleInfo {
                ip: Some(resp.own_ip.ok_or::<Error>("No IP found".into())?),
                auth_url: None,
            })
        } else {
            Ok(TailscaleInfo {
                ip: None,
                auth_url: Some(resp.auth_url),
            })
        }
    }
}
