use async_graphql::connection::{query, Connection, Edge};
use async_graphql::{ComplexObject, Context, Error, Result, SimpleObject};
use biscuit::jwa::SignatureAlgorithm;
use biscuit::jws::{RegisteredHeader, Secret};
use biscuit::{ClaimsSet, RegisteredClaims, JWT};
use tokio::sync::OnceCell;

use crate::api::apps::StoreRef;
use crate::api::core::EntropyRequest;
use crate::apps::types::AppMetadata;
use crate::apps::{get_app_registry, get_stores_yml};
use crate::graphql::auth_ctx::UserCtx;
use crate::graphql::auth_guard::PermissionGuard;
use crate::graphql::clients::{entropy_client, manage_client};
use crate::graphql::query::apps::store::AppStore;
use crate::graphql::query::https::acme_provider::AcmeProvider;
use crate::graphql::query::https::app_domain::AppDomain;
use crate::graphql::query::https::proxy::{Proxy, Service};
use crate::graphql::query::https::user_domain::UserDomain;
use crate::graphql::query::nirvati_me;
use crate::graphql::query::nirvati_me::NirvatiMeDetails;
use crate::prisma;
use crate::prisma::app_domain;
use crate::prisma::read_filters::StringFilter;
use crate::prisma::{user, PrismaClient};
use crate::utils::jwts::JwtAdditionalClaims;
use crate::utils::user_state::get_user_state;

use super::Permission;

user::select!(user_metadata {
    username
    name
    email
    confirmed_email
    created_at
    is_enabled
    acme_accounts: select {
        provider
    }
    permissions
});

impl From<user_metadata::Data> for UserCache {
    fn from(data: user_metadata::Data) -> Self {
        let mut acme_providers = vec![];
        for acme_account in data.acme_accounts {
            acme_providers.push(acme_account.provider.into());
        }
        UserCache {
            name: data.name,
            permissions: data.permissions,
            email: data.email,
            last_verified_email: data.confirmed_email,
            created_at: data.created_at.into(),
            enabled: data.is_enabled,
            acme_providers,
        }
    }
}

impl From<user_metadata::Data> for User {
    fn from(data: user_metadata::Data) -> Self {
        User {
            username: data.username.to_owned(),
            _cache: OnceCell::new_with(Some(data.into())),
        }
    }
}

#[derive(Clone)]
pub struct UserCache {
    pub name: String,
    pub permissions: Vec<crate::prisma::Permission>,
    pub email: Option<String>,
    pub last_verified_email: Option<String>,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub enabled: bool,
    pub acme_providers: Vec<AcmeProvider>,
}

#[derive(Clone, SimpleObject)]
#[graphql(complex)]
pub struct User {
    pub username: String,
    #[graphql(skip)]
    pub _cache: OnceCell<UserCache>,
}

#[ComplexObject]
impl User {
    #[graphql(skip)]
    pub fn from_username(name: String) -> Self {
        Self {
            username: name,
            _cache: OnceCell::new(),
        }
    }

    // To get access to a user object, the authorization is checked already, so we don't need a check here
    async fn domains(&self, ctx: &Context<'_>) -> Result<Vec<UserDomain>> {
        let user_domains = ctx
            .data::<PrismaClient>()?
            .user()
            .find_unique(user::username::equals(self.username.clone()))
            .select(user::select!({
                domains: select {
                    created_at
                    name
                    provider
                    provider_auth
                    owner_name
                }
            }))
            .exec()
            .await
            .map_err(|_| Error::new("User not found"))?
            .ok_or(Error::new("User not found"))?;
        Ok(user_domains
            .domains
            .into_iter()
            .map(|domain| UserDomain {
                created_at: domain.created_at.into(),
                domain: domain.name,
                dns_provider: domain.provider.into(),
                dns_provider_auth: domain.provider_auth,
                owner_name: domain.owner_name,
            })
            .collect())
    }

    async fn app_domains(
        &self,
        ctx: &Context<'_>,
        after: Option<String>,
        before: Option<String>,
        first: Option<i32>,
        last: Option<i32>,
    ) -> Result<Connection<usize, AppDomain>> {
        query(
            after,
            before,
            first,
            last,
            |after, before, first, last| async move {
                let mut start = 0usize;
                let total = ctx
                    .data::<PrismaClient>()?
                    .app_domain()
                    .count(vec![
                        crate::prisma::app_domain::WhereParam::AppInstallationIs(vec![
                            crate::prisma::app_installation::owner_name::equals(
                                self.username.clone(),
                            ),
                        ]),
                    ])
                    .exec()
                    .await
                    .map_err(|_| Error::new("Failed to count app domains"))?
                    as usize;
                let mut end = total;
                if let Some(after) = after {
                    if after >= total {
                        let has_previous_page = (after - start) > 0;
                        return Ok(Connection::new(has_previous_page, false));
                    }
                    start = after + 1;
                }
                if let Some(before) = before {
                    if before == 0 {
                        return Ok(Connection::new(false, false));
                    }
                    end = before;
                }
                if let Some(first) = first {
                    let new_end = start + first;
                    if new_end < end {
                        end = new_end;
                    }
                }
                if let Some(last) = last {
                    let new_start = end - last;
                    if new_start > start {
                        start = new_start;
                    }
                }

                ctx.data::<PrismaClient>()?
                    .app_domain()
                    .find_many(vec![
                        crate::prisma::app_domain::WhereParam::AppInstallationIs(vec![
                            crate::prisma::app_installation::owner_name::equals(
                                self.username.clone(),
                            ),
                        ]),
                    ])
                    .skip(start as i64)
                    .take((end - start) as i64)
                    .select(app_domain::select!(
                        {
                            name
                            created_at
                            app_installation: select {
                                app_id
                                owner_name
                            }
                            parent_domain_name
                        }
                    ))
                    .exec()
                    .await
                    .map(|app_domains| {
                        let mut edges = vec![];
                        for app_domain in app_domains {
                            edges.push(Edge::new(
                                start,
                                AppDomain {
                                    name: app_domain.name,
                                    created_at: app_domain.created_at.into(),
                                    app_id: app_domain.app_installation.app_id,
                                    owner_name: app_domain.app_installation.owner_name,
                                    parent: app_domain.parent_domain_name,
                                },
                            ));
                        }
                        let mut connection = Connection::new(start > 0, end < total);
                        connection.edges.append(&mut edges);
                        connection
                    })
                    .map_err(|_| Error::new("Failed to find users"))
            },
        )
        .await
    }

    #[graphql(skip)]
    async fn cache(&self, ctx: &Context<'_>) -> Result<&UserCache> {
        self._cache
            .get_or_try_init(|| async {
                let user = ctx
                    .data::<PrismaClient>()?
                    .user()
                    .find_unique(user::username::equals(self.username.clone()))
                    .select(user_metadata::select())
                    .exec()
                    .await
                    .map_err(|_| Error::new("User not found"))?
                    .ok_or(Error::new("User not found"))?;
                Ok(user.into())
            })
            .await
    }

    async fn name(&self, ctx: &Context<'_>) -> Result<String> {
        Ok(self.cache(ctx).await?.name.clone())
    }

    async fn permissions(&self, ctx: &Context<'_>) -> Result<Vec<Permission>> {
        Ok(self
            .cache(ctx)
            .await?
            .permissions
            .iter()
            .map(|p| (*p).into())
            .collect())
    }
    async fn email(&self, ctx: &Context<'_>) -> Result<Option<String>> {
        Ok(self.cache(ctx).await?.email.clone())
    }
    async fn last_verified_email(&self, ctx: &Context<'_>) -> Result<Option<String>> {
        Ok(self.cache(ctx).await?.last_verified_email.clone())
    }
    async fn created_at(&self, ctx: &Context<'_>) -> Result<chrono::DateTime<chrono::Utc>> {
        Ok(self.cache(ctx).await?.created_at)
    }
    async fn enabled(&self, ctx: &Context<'_>) -> Result<bool> {
        Ok(self.cache(ctx).await?.enabled)
    }
    async fn acme_providers(&self, ctx: &Context<'_>) -> Result<Vec<AcmeProvider>> {
        Ok(self.cache(ctx).await?.acme_providers.clone())
    }

    async fn stores(&self, ctx: &Context<'_>) -> Result<Vec<AppStore>> {
        get_stores_yml(&ctx.data_unchecked::<UserCtx>().username).map_err::<Error, _>(|err| {
            Error::new(format!(
                "Failed to get stores for user {}: {}",
                self.username, err
            ))
        })
    }

    async fn store(&self, ctx: &Context<'_>, id: String) -> Result<AppStore> {
        get_stores_yml(&ctx.data_unchecked::<UserCtx>().username)
            .map_err::<Error, _>(|err| {
                Error::new(format!(
                    "Failed to get stores for user {}: {}",
                    self.username, err
                ))
            })?
            .into_iter()
            .find(|store| store.id == id)
            .ok_or(Error::new("Store not found"))
    }

    async fn apps(
        &self,
        ctx: &Context<'_>,
        installed_only: Option<bool>,
    ) -> Result<Vec<AppMetadata>> {
        let installed_only = installed_only.unwrap_or(false);
        let prisma_client = ctx.data_unchecked::<PrismaClient>();
        let installed_apps = prisma_client
            .app_installation()
            .find_many(vec![prisma::app_installation::owner_name::equals(
                self.username.clone(),
            )])
            .select(prisma::app_installation::select!({ app_id }))
            .exec()
            .await
            .map_err::<Error, _>(|err| err.to_string().into())?
            .into_iter()
            .map(|app| app.app_id)
            .collect::<Vec<String>>();
        let mut app_registry =
            get_app_registry(&self.username, &installed_apps).map_err(|err| {
                tracing::warn!("{:#?}", err);
                Error::new("Failed to get app registry")
            })?;
        if ctx.look_ahead().field("latestVersion").exists() {
            let mut manage_client = manage_client(ctx)?;
            let all_updates = manage_client
                .get_updates(tonic::Request::new(crate::api::apps::GetAppUpdateRequest {
                    reference: Some(crate::api::apps::get_app_update_request::Reference::Store(
                        StoreRef {
                            new_or_not_installed_only: false,
                            store_src: None,
                        },
                    )),
                    user_state: get_user_state(ctx).await,
                }))
                .await
                .map_err(|_| Error::new("Failed to get app updates"))?
                .into_inner()
                .updates;
            for app in &mut app_registry {
                if let Some(update) = all_updates.get(&app.id) {
                    app.latest_version
                        .set(Some(update.clone().into()))
                        .map_err::<Error, _>(|_| Error::new("Failed to set latest version"))?;
                }
            }
        }
        if installed_only {
            Ok(app_registry
                .into_iter()
                .filter(|app| app.installed)
                .collect())
        } else {
            Ok(app_registry)
        }
    }

    async fn app(&self, ctx: &Context<'_>, app_id: String) -> Result<AppMetadata> {
        let prisma_client = ctx.data_unchecked::<PrismaClient>();
        let is_installed = prisma_client
            .app_installation()
            .find_unique(
                prisma::app_installation::UniqueWhereParam::AppIdOwnerNameEquals(
                    app_id.clone(),
                    self.username.clone(),
                ),
            )
            .exec()
            .await
            .map(|app| app.is_some())
            .unwrap_or(false);
        let installed_apps = if is_installed {
            vec![app_id.clone()]
        } else {
            vec![]
        };
        get_app_registry(&self.username, &installed_apps)
            .map_err(|_| Error::new("Failed to get app registry"))?
            .into_iter()
            .find(|app| app.id == app_id)
            .ok_or(Error::new("App not found"))
    }

    async fn jwt(&self, ctx: &Context<'_>) -> Result<String> {
        let client = ctx.data::<PrismaClient>()?;
        // We do another call to find to make sure a deleted user can not keep issueing JWTs for themselves
        // This can be bypassed by Instance admins, who can currently issue JWTs for other admins
        // We will fix that long-term, but for now it's not a big issue considering that instance admin permissions are not given out lightly
        let user = client
            .user()
            .find_unique(user::UniqueWhereParam::UsernameEquals(
                self.username.clone(),
            ))
            .exec()
            .await
            .map_err(|_| Error::new("Failed to find user"))?
            .ok_or(Error::new("Failed to find user"))?;
        let secret = ctx.data_unchecked::<Secret>();
        let claims = ClaimsSet::<JwtAdditionalClaims> {
            registered: RegisteredClaims {
                issuer: Some("nirvati".to_string()),
                subject: Some(self.username.clone()),
                issued_at: Some(chrono::Utc::now().into()),
                expiry: Some(
                    // TODO: Make expiry configurable
                    (chrono::Utc::now() + chrono::Duration::minutes(10)).into(),
                ),
                ..Default::default()
            },
            private: JwtAdditionalClaims {
                user_group: user.user_group_id,
                permission_set: user.permissions.as_slice().into(),
                email: user.email,
            },
        };
        let token = JWT::new_decoded(
            From::from(RegisteredHeader {
                algorithm: SignatureAlgorithm::HS256,
                ..Default::default()
            }),
            claims.clone(),
        );

        let token = token
            .into_encoded(secret)
            .map_err(|_| Error::new("Failed to encode token"))?;
        Ok(token.unwrap_encoded().to_string())
    }

    pub async fn entropy(&self, ctx: &Context<'_>, identifier: String) -> Result<String> {
        let mut client = entropy_client(ctx)?;
        client
            .derive_entropy(tonic::Request::new(EntropyRequest {
                input: format!("{}:{}", self.username, identifier),
            }))
            .await
            .map(|response| response.into_inner().entropy)
            .map_err::<Error, _>(|_| Error::new("Failed to derive entropy"))
    }

    #[graphql(guard = "PermissionGuard::new(crate::prisma::Permission::UseNirvatiMe)")]
    async fn nirvati_me(&self, ctx: &Context<'_>) -> Result<Option<NirvatiMeDetails>> {
        let mut client = entropy_client(ctx)?;
        let username = client
            .derive_entropy(tonic::Request::new(EntropyRequest {
                input: format!("{}:nirvati.me-username", self.username),
            }))
            .await
            .map(|response| response.into_inner().entropy)
            .map_err::<Error, _>(|_| Error::new("Failed to derive entropy"))?;
        let password = client
            .derive_entropy(tonic::Request::new(EntropyRequest {
                input: format!("{}:nirvati.me-password", self.username),
            }))
            .await
            .map(|response| response.into_inner().entropy)
            .map_err::<Error, _>(|_| Error::new("Failed to derive entropy"))?;
        let result = nirvati_me::get_details(&username, &password).await;
        if let Err(err) = &result {
            tracing::info!("Failed to get Nirvati.me details: {}", err);
            Ok(None)
        } else {
            Ok(nirvati_me::get_details(&username, &password).await.ok())
        }
    }

    async fn proxies(&self, ctx: &Context<'_>) -> Result<Vec<Proxy>> {
        let prisma_client = ctx.data_unchecked::<PrismaClient>();
        prisma_client
            .proxy()
            .find_many(vec![prisma::proxy::WhereParam::OwnerName(
                StringFilter::Equals(self.username.clone()),
            )])
            .exec()
            .await
            .map_err(|_| Error::new("Failed to find proxies"))
            .map(|proxies| {
                proxies
                    .into_iter()
                    .map(|proxy| Proxy {
                        domain: proxy.domain,
                        owner: proxy.owner_name,
                    })
                    .collect()
            })
    }

    async fn proxy(&self, ctx: &Context<'_>, domain: String) -> Result<Proxy> {
        let prisma_client = ctx.data_unchecked::<PrismaClient>();
        prisma_client
            .proxy()
            .find_unique(prisma::proxy::UniqueWhereParam::DomainEquals(domain))
            .exec()
            .await
            .map_err(|_| Error::new("Failed to find proxy"))?
            .map(|proxy| Proxy {
                domain: proxy.domain,
                owner: proxy.owner_name,
            })
            .ok_or(Error::new("Failed to find proxy"))
    }

    async fn services(&self, ctx: &Context<'_>) -> Result<Vec<Service>> {
        let prisma_client = ctx.data_unchecked::<PrismaClient>();
        prisma_client
            .service()
            .find_many(vec![prisma::service::WhereParam::OwnerIs(vec![
                user::WhereParam::Username(StringFilter::Equals(self.username.clone())),
            ])])
            .exec()
            .await
            .map_err(|_| Error::new("Failed to find services"))
            .map(|services| {
                services
                    .into_iter()
                    .map(|service| Service {
                        id: service.id,
                        owner: service.owner_name,
                        upstream: service.upstream,
                        upstream_type: service.r#type.into(),
                        tcp_ports: service.tcp_ports.into_iter().map(|p| p as u16).collect(),
                        udp_ports: service.udp_ports.into_iter().map(|p| p as u16).collect(),
                    })
                    .collect()
            })
    }

    async fn service(&self, ctx: &Context<'_>, id: String) -> Result<Service> {
        let prisma_client = ctx.data_unchecked::<PrismaClient>();
        prisma_client
            .service()
            .find_unique(prisma::service::UniqueWhereParam::IdEquals(id))
            .exec()
            .await
            .map_err(|_| Error::new("Failed to find service"))?
            .and_then(|service| {
                if &service.owner_name == &self.username {
                    Some(Service {
                        id: service.id,
                        owner: service.owner_name,
                        upstream: service.upstream,
                        upstream_type: service.r#type.into(),
                        tcp_ports: service.tcp_ports.into_iter().map(|p| p as u16).collect(),
                        udp_ports: service.udp_ports.into_iter().map(|p| p as u16).collect(),
                    })
                } else {
                    None
                }
            })
            .ok_or(Error::new("Failed to find service"))
    }
}
