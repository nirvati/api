use async_graphql::Enum;
use serde::{Deserialize, Serialize};

#[derive(Enum, Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[repr(u8)]
#[graphql(remote = "crate::prisma::Permission")]
pub enum Permission {
    ManageApps,
    ManageDomains,
    ManageUsers,
    ManageGroups,
    ManageProxies,
    ManageIssuers,
    ConfigureNetwork,
    ManageSysComponents,
    ManageHardware,
    Escalate,
    UseNirvatiMe,
    ManageAppStores,
    ManageAppStorage,
}
