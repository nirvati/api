use std::collections::HashMap;

use async_graphql::{ComplexObject, Context, Error, Json, SimpleObject};
use k8s_openapi::api::core::v1::PersistentVolumeClaim;
use kube::Api;

use crate::apps::types::{AppMetadata, Volume as VolumeDefinition};
use crate::graphql::clients::manage_client;
use crate::graphql::query::https::app_domain::AppDomain;
use crate::graphql::query::users::User;
use crate::graphql::query::utils::MultiLangItem;
use crate::longhorn::Client as LonghornClient;
use crate::prisma::{app_domain, app_installation};
use crate::utils::user_state::get_user_state;

#[derive(SimpleObject, Clone, Debug, PartialEq, Eq)]
pub struct Volume {
    pub id: String,
    pub size: u64,
    pub replica_count: u32,
    pub actual_size: u64,
    pub human_readable_id: String,
    pub volume_definition: VolumeDefinition,
}

#[derive(SimpleObject, Clone, Debug, PartialEq, Eq)]
pub struct AppUpdate {
    latest_version: String,
    releases_notes: Json<HashMap<String, MultiLangItem>>,
}

impl From<crate::api::apps::AppUpdate> for AppUpdate {
    fn from(update: crate::api::apps::AppUpdate) -> Self {
        Self {
            latest_version: update.latest,
            releases_notes: Json(
                update
                    .releases_notes
                    .into_iter()
                    .map(|(k, v)| (k, MultiLangItem(v.notes.into_iter().collect())))
                    .collect(),
            ),
        }
    }
}

#[ComplexObject]
impl AppMetadata {
    async fn owner(&self) -> User {
        User::from_username(self.owner.clone())
    }
    async fn latest_version(&self, ctx: &Context<'_>) -> async_graphql::Result<Option<AppUpdate>> {
        if let Some(latest_version) = self.latest_version.get() {
            Ok(latest_version.clone())
        } else {
            // Basically unreachable, but if this becomes reachable in the future, prevent a crash
            let mut client = manage_client(ctx)?;
            let response = client
                .get_updates(tonic::Request::new(crate::api::apps::GetAppUpdateRequest {
                    reference: Some(crate::api::apps::get_app_update_request::Reference::App(
                        self.id.clone(),
                    )),
                    user_state: get_user_state(ctx).await,
                }))
                .await
                .map_err(|_| Error::new("Failed to get app updates"))?
                .into_inner();
            if let Some(update) = response.updates.get(&self.id) {
                if update.current == update.latest {
                    Ok(None)
                } else {
                    Ok(Some(update.clone().into()))
                }
            } else {
                Ok(None)
            }
        }
    }

    async fn domains(&self, ctx: &Context<'_>) -> async_graphql::Result<Vec<AppDomain>> {
        let domains = ctx
            .data::<crate::prisma::PrismaClient>()?
            .app_domain()
            .find_many(vec![app_domain::app_installation::is(vec![
                app_installation::owner_name::equals(self.owner.clone()),
                app_installation::app_id::equals(self.id.clone()),
            ])])
            .exec()
            .await
            .map_err(|_| Error::new("Failed to get app domains"))?;
        Ok(domains
            .into_iter()
            .map(|app_domain| AppDomain {
                name: app_domain.name,
                created_at: app_domain.created_at.into(),
                app_id: self.id.clone(),
                owner_name: self.owner.clone(),
                parent: app_domain.parent_domain_name,
            })
            .collect())
    }

    async fn running_volumes(&self, ctx: &Context<'_>) -> async_graphql::Result<Vec<Volume>> {
        let kube_client = ctx.data_unchecked::<kube::Client>().clone();
        let longhorn_client = ctx.data_unchecked::<LonghornClient>();
        let app_ns = format!("{}-{}", self.owner, self.id);
        let volumes_api: Api<PersistentVolumeClaim> = Api::namespaced(kube_client, &app_ns);
        let volumes = volumes_api.list(&Default::default()).await.map_err(|err| {
            tracing::error!("Failed to get app volumes: {:?}", err);
            Error::new("Failed to get app volumes")
        })?;
        let mut result = vec![];
        for volume in volumes {
            let Some(ref name) = volume.metadata.name else {
                continue;
            };
            let Some(main_definition) = self.volumes.get(name) else {
                continue;
            };
            let Some(spec) = volume.spec else {
                continue;
            };
            let Some(inner) = spec.volume_name else {
                continue;
            };
            let details = longhorn_client.get_volume(&inner).await?;
            result.push(Volume {
                id: inner,
                size: details
                    .size
                    .parse()
                    .map_err(|_| Error::new("Failed to parse volume size"))?,
                replica_count: details.number_of_replicas as u32,
                actual_size: details
                    .controllers
                    .first()
                    .map(|c| c.actual_size.clone())
                    .unwrap_or("0".to_string())
                    .parse()
                    .map_err(|_| Error::new("Failed to parse volume size"))?,
                human_readable_id: name.clone(),
                volume_definition: main_definition.clone(),
            });
        }
        return Ok(result);
    }
}
