use std::collections::BTreeMap;

use async_graphql::{ComplexObject, Context, Enum, Error, Json, Result, SimpleObject};
use serde::{Deserialize, Serialize};

use crate::api::apps::StoreRef;
use crate::apps::get_app_registry;
use crate::apps::types::AppMetadata;
use crate::graphql::clients::manage_client;
use crate::graphql::query::apps::app::AppUpdate;
use crate::graphql::query::users::User;
use crate::graphql::query::utils::MultiLangItem;
use crate::prisma;
use crate::prisma::PrismaClient;
use crate::utils::user_state::get_user_state;

#[derive(Enum, Clone, Copy, Serialize, Deserialize, Debug, PartialEq, Eq, Default)]
#[graphql(remote = "crate::api::apps::StoreType")]
pub enum StoreType {
    #[default]
    Nirvati,
    Plugin,
}

#[derive(SimpleObject, Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
#[graphql(complex)]
pub struct AppStore {
    pub id: String,
    pub name: MultiLangItem,
    pub tagline: MultiLangItem,
    pub description: MultiLangItem,
    pub icon: String,
    // Developer name -> their website
    pub developers: Json<BTreeMap<String, String>>,
    pub license: String,
    #[serde(default)]
    pub r#type: StoreType,
    pub src: String,
    #[graphql(skip)]
    pub apps: Vec<String>,

    #[graphql(skip)]
    #[serde(skip)]
    pub owner: String,

    // Data specific to the app store type
    #[graphql(skip)]
    pub provider_data: serde_yaml::Value,
}

#[ComplexObject]
impl AppStore {
    async fn apps(&self, ctx: &Context<'_>) -> Result<Vec<AppMetadata>> {
        let prisma_client = ctx.data_unchecked::<PrismaClient>();
        let installed_apps = prisma_client
            .app_installation()
            .find_many(vec![prisma::app_installation::owner_name::equals(
                self.owner.clone(),
            )])
            .select(prisma::app_installation::select!({ app_id }))
            .exec()
            .await
            .map_err::<Error, _>(|err| err.to_string().into())?
            .into_iter()
            .map(|app| app.app_id)
            .collect::<Vec<String>>();
        let mut registry = get_app_registry(&self.owner, &installed_apps)
            .map_err(|_| Error::new("App registry not found"))?;
        let mut manage_client = manage_client(ctx)?;
        let response = manage_client
            .get_updates(tonic::Request::new(crate::api::apps::GetAppUpdateRequest {
                reference: Some(crate::api::apps::get_app_update_request::Reference::Store(
                    StoreRef {
                        new_or_not_installed_only: false,
                        store_src: Some(self.src.clone()),
                    },
                )),
                user_state: get_user_state(ctx).await,
            }))
            .await
            .map_err(|_| Error::new("Failed to get app updates"))?
            .into_inner();
        let mut updates: BTreeMap<String, AppUpdate> = BTreeMap::new();
        for (app_id, update) in response.updates {
            if update.current != update.latest {
                updates.insert(app_id, update.into());
            }
        }
        for app in &mut registry {
            if let Some(update) = updates.get(&app.id) {
                app.latest_version
                    .set(Some(update.clone()))
                    .map_err::<Error, _>(|_| Error::new("Failed to set latest version"))?;
            }
        }
        Ok(registry
            .into_iter()
            .filter(|app| self.apps.contains(&app.id))
            .collect())
    }

    async fn owner(&self) -> Result<User> {
        Ok(User::from_username(self.owner.clone()))
    }
}
