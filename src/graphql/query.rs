use async_graphql::Object;

use crate::prisma::Permission;

use super::auth_guard::{IsLoggedInGuard, PermissionGuard};

pub(crate) mod apps;
pub(crate) mod https;
mod info;
mod network;
mod nirvati_me;
mod storage;
mod upgrades;
pub(crate) mod users;
pub(crate) mod utils;

pub struct Query;

#[Object]
impl Query {
    // Network methods are typically only used during setup by the instance admin
    // We can reevaluate this later if we want to expose some of this to other users
    #[graphql(guard = "PermissionGuard::new(Permission::ConfigureNetwork)")]
    async fn network(&self) -> network::Network {
        network::Network
    }
    async fn info(&self) -> info::Info {
        info::Info
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageSysComponents)")]
    async fn upgrades(&self) -> upgrades::Upgrades {
        upgrades::Upgrades
    }

    /*async fn https(&self) -> https::Https {
        https::Https
    }*/

    #[graphql(guard = "IsLoggedInGuard::new()")]
    async fn users(&self) -> users::Users {
        users::Users
    }
}
