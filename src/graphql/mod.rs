use async_graphql::{EmptySubscription, Schema};

pub use mutation::Mutation;
pub use query::Query;

pub mod auth_ctx;
mod auth_guard;
mod clients;
mod instance_type_guard;
mod mutation;
pub(crate) mod query;

pub type AppSchema = Schema<Query, Mutation, EmptySubscription>;

pub struct ChannelCtx {
    pub(crate) core_channel: tonic::transport::Channel,
    pub(crate) apps_channel: tonic::transport::Channel,
    pub(crate) https_channel: tonic::transport::Channel,
}

impl ChannelCtx {
    pub fn new(
        core_channel: tonic::transport::Channel,
        apps_channel: tonic::transport::Channel,
        https_channel: tonic::transport::Channel,
    ) -> Self {
        Self {
            core_channel,
            apps_channel,
            https_channel,
        }
    }
}
