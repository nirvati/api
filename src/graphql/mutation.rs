use async_graphql::MergedObject;

mod apps;
mod https;
mod network;
mod proxies;
mod services;
mod setup;
mod storage;
mod upgrades;
mod user;

#[derive(MergedObject, Default)]
pub struct Mutation(
    setup::Setup,
    user::User,
    upgrades::Upgrades,
    network::Network,
    apps::Apps,
    https::Https,
    storage::Storage,
    services::Services,
    proxies::Proxies,
);
