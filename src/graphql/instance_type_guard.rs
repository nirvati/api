use async_graphql::{Context, Guard, Result};

use crate::instance::{InstanceType, INSTANCE_TYPE};

pub struct InstanceTypeGuard {
    instance_type: InstanceType,
    is_inverse: bool,
}

impl InstanceTypeGuard {
    pub fn new(instance_type: InstanceType) -> Self {
        Self {
            instance_type,
            is_inverse: false,
        }
    }
    pub fn new_inverse(instance_type: InstanceType) -> Self {
        Self {
            instance_type,
            is_inverse: true,
        }
    }
}

impl Guard for InstanceTypeGuard {
    async fn check(&self, ctx: &Context<'_>) -> Result<()> {
        let mut is_instance_type = ctx.data_opt::<InstanceType>() == Some(&self.instance_type);
        if self.is_inverse {
            is_instance_type = !is_instance_type;
        };
        if is_instance_type {
            Ok(())
        } else {
            Err("Method not available".into())
        }
    }
}
