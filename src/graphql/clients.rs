use async_graphql::{Context, Error};
use tonic::transport::Channel;

use crate::api::apps::apps_client::AppsClient;
use crate::api::apps::manager_client::ManagerClient;
use crate::api::apps::setup_client::SetupClient;
use crate::api::core::entropy_client::EntropyClient;
use crate::api::core::middlewares_client::MiddlewaresClient;
use crate::api::core::node_client::NodeClient;
use crate::api::core::services_client::ServicesClient;
use crate::api::core::tailscale_client::TailscaleClient;
use crate::api::core::upgrades_client::UpgradesClient;
use crate::api::core::users_client::UsersClient;
use crate::api::https::https_client::HttpsClient;
use crate::graphql::ChannelCtx;

pub fn apps_client(ctx: &Context<'_>) -> async_graphql::Result<AppsClient<Channel>> {
    let channel = ctx
        .data_opt::<ChannelCtx>()
        .ok_or::<Error>("No communication channel found".into())?
        .apps_channel
        .clone();
    Ok(AppsClient::new(channel))
}

pub fn manage_client(ctx: &Context<'_>) -> async_graphql::Result<ManagerClient<Channel>> {
    let channel = ctx
        .data_opt::<ChannelCtx>()
        .ok_or::<Error>("No communication channel found".into())?
        .apps_channel
        .clone();
    Ok(ManagerClient::new(channel))
}

pub fn https_client(ctx: &Context<'_>) -> async_graphql::Result<HttpsClient<Channel>> {
    let channel = ctx
        .data_opt::<ChannelCtx>()
        .ok_or::<Error>("No communication channel found".into())?
        .https_channel
        .clone();
    Ok(HttpsClient::new(channel))
}

pub fn node_client(ctx: &Context<'_>) -> async_graphql::Result<NodeClient<Channel>> {
    let channel = ctx
        .data_opt::<ChannelCtx>()
        .ok_or::<Error>("No communication channel found".into())?
        .core_channel
        .clone();
    Ok(NodeClient::new(channel))
}

pub fn tailscale_client(ctx: &Context<'_>) -> async_graphql::Result<TailscaleClient<Channel>> {
    let channel = ctx
        .data_opt::<ChannelCtx>()
        .ok_or::<Error>("No communication channel found".into())?
        .core_channel
        .clone();
    Ok(TailscaleClient::new(channel))
}

pub fn upgrades_client(ctx: &Context<'_>) -> async_graphql::Result<UpgradesClient<Channel>> {
    let channel = ctx
        .data_opt::<ChannelCtx>()
        .ok_or::<Error>("No communication channel found".into())?
        .core_channel
        .clone();
    Ok(UpgradesClient::new(channel))
}

pub fn entropy_client(ctx: &Context<'_>) -> async_graphql::Result<EntropyClient<Channel>> {
    let channel = ctx
        .data_opt::<ChannelCtx>()
        .ok_or::<Error>("No communication channel found".into())?
        .core_channel
        .clone();
    Ok(EntropyClient::new(channel))
}

pub fn setup_client(ctx: &Context<'_>) -> async_graphql::Result<SetupClient<Channel>> {
    let channel = ctx
        .data_opt::<ChannelCtx>()
        .ok_or::<Error>("No communication channel found".into())?
        .apps_channel
        .clone();
    Ok(SetupClient::new(channel))
}

pub fn users_client(ctx: &Context<'_>) -> async_graphql::Result<UsersClient<Channel>> {
    let channel = ctx
        .data_opt::<ChannelCtx>()
        .ok_or::<Error>("No communication channel found".into())?
        .core_channel
        .clone();
    Ok(UsersClient::new(channel))
}

pub fn services_client(ctx: &Context<'_>) -> async_graphql::Result<ServicesClient<Channel>> {
    let channel = ctx
        .data_opt::<ChannelCtx>()
        .ok_or::<Error>("No communication channel found".into())?
        .core_channel
        .clone();
    Ok(ServicesClient::new(channel))
}

pub fn proxies_client(
    ctx: &Context<'_>,
) -> async_graphql::Result<crate::api::core::proxies_client::ProxiesClient<Channel>> {
    let channel = ctx
        .data_opt::<ChannelCtx>()
        .ok_or::<Error>("No communication channel found".into())?
        .core_channel
        .clone();
    Ok(crate::api::core::proxies_client::ProxiesClient::new(
        channel,
    ))
}

pub fn middlewares_client(ctx: &Context<'_>) -> async_graphql::Result<MiddlewaresClient<Channel>> {
    let channel = ctx
        .data_opt::<ChannelCtx>()
        .ok_or::<Error>("No communication channel found".into())?
        .core_channel
        .clone();
    Ok(MiddlewaresClient::new(channel))
}
