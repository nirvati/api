use async_graphql::{Context, Guard, Result};

use crate::graphql::auth_ctx::UserCtx;
use crate::prisma::Permission;

pub struct PermissionGuard {
    required: Permission,
}

impl PermissionGuard {
    pub fn new(required: Permission) -> Self {
        Self { required }
    }
}

impl Guard for PermissionGuard {
    async fn check(&self, ctx: &Context<'_>) -> Result<()> {
        if ctx
            .data_opt::<UserCtx>()
            .map(|ctx| ctx.permissions.has(self.required))
            .unwrap_or(false)
        {
            Ok(())
        } else {
            Err("Forbidden".into())
        }
    }
}

pub struct IsLoggedInGuard;

impl IsLoggedInGuard {
    pub fn new() -> Self {
        Self {}
    }
}

impl Guard for IsLoggedInGuard {
    async fn check(&self, ctx: &Context<'_>) -> Result<()> {
        if ctx.data_opt::<UserCtx>().is_some() {
            Ok(())
        } else {
            Err("Forbidden".into())
        }
    }
}
