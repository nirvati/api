pub mod core {
    tonic::include_proto!("nirvati");
}

pub mod apps {
    tonic::include_proto!("apps");
}

pub mod https {
    tonic::include_proto!("nirvati_https");
}
