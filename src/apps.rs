use std::fs::File;
use std::path::Path;

use crate::apps::types::{AppMetadata, OutputDependency};
use crate::graphql::query::apps::store::AppStore;

// TODO: The code in types has been copied and slightly modified from the app manager
// It should probably be refactored into a common crate
pub mod types;

pub fn finalize_app(
    mut app: AppMetadata,
    user: &str,
    installed_apps: &[String],
    apps_by_id: &std::collections::HashMap<String, AppMetadata>,
    previously_visited: &mut Vec<String>,
) -> Option<AppMetadata> {
    // The app manager should have already checked that there are no circular dependencies, so this should be safe
    // But we have a check here just in case
    if previously_visited.contains(&app.id) {
        return None;
    }
    previously_visited.push(app.id.clone());
    app.owner = user.to_string();
    app.installed = installed_apps.contains(&app.id);
    app.dependencies_resolved = app
        .dependencies
        .iter()
        .filter_map(|dep| {
            Some(match dep {
                types::Dependency::OneDependency(id) => {
                    let target_app = apps_by_id.get(id)?.clone();
                    types::OutputDependency::OneDependency(finalize_app(
                        target_app,
                        user,
                        installed_apps,
                        apps_by_id,
                        previously_visited,
                    )?)
                }
                types::Dependency::AlternativeDependency(ids) => {
                    types::OutputDependency::AlternativeDependency(types::AlternativeDependency {
                        options: ids
                            .iter()
                            .filter_map(|id| {
                                let target_app = apps_by_id.get(id)?.clone();
                                finalize_app(
                                    target_app,
                                    user,
                                    installed_apps,
                                    apps_by_id,
                                    previously_visited,
                                )
                            })
                            .collect(),
                    })
                }
            })
        })
        .collect();
    if app.dependencies_resolved.len() != app.dependencies.len() {
        return None;
    }
    if app
        .dependencies_resolved
        .iter()
        .filter_map(|dep| match dep {
            OutputDependency::OneDependency(_) => None,
            OutputDependency::AlternativeDependency(ids) => Some(&ids.options),
        })
        .any(|ids| ids.is_empty())
    {
        return None;
    };
    for dep in app.dependencies_resolved.iter_mut() {
        if let OutputDependency::AlternativeDependency(alt_dep) = dep {
            if alt_dep.options.len() == 1 {
                *dep = OutputDependency::OneDependency(alt_dep.options.remove(0));
            }
        }
    }
    Some(app)
}

/// Read the app registry
pub fn get_app_registry(user: &str, installed_apps: &[String]) -> anyhow::Result<Vec<AppMetadata>> {
    let app_registry_path = Path::new(&std::env::var("APPS_DIR")?)
        .join(user)
        .join("registry.json");
    let app_registry = File::open(app_registry_path)?;
    let app_registry: Vec<AppMetadata> = serde_json::from_reader(app_registry)?;
    let apps_by_id = app_registry
        .iter()
        .map(|app| (app.id.clone(), app.clone()))
        .collect::<std::collections::HashMap<_, _>>();
    let app_registry = app_registry
        .into_iter()
        .filter_map(|app| finalize_app(app, user, installed_apps, &apps_by_id, &mut vec![]))
        .collect();
    Ok(app_registry)
}

pub fn get_stores_yml(user: &str) -> anyhow::Result<Vec<AppStore>> {
    let stores_yml_path = Path::new(&std::env::var("APPS_DIR")?)
        .join(user)
        .join("stores.yml");
    let stores_yml = File::open(stores_yml_path)?;
    let mut result: Vec<AppStore> = serde_yaml::from_reader(stores_yml)?;
    for store in result.iter_mut() {
        store.owner = user.to_string();
    }
    Ok(result)
}
