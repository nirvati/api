use std::collections::{BTreeMap, HashMap};
use std::sync::OnceLock;

use async_graphql::{Enum, Json, SimpleObject, Union};
use serde::{Deserialize, Serialize};

use crate::graphql::query::apps::app::AppUpdate;
use crate::graphql::query::utils::MultiLangItem;

#[derive(Enum, Deserialize, Serialize, Clone, Debug, PartialEq, Eq, Copy)]
#[serde(rename_all = "snake_case")]
pub enum SettingType {
    Enum,
    String,
    Bool,
}

#[derive(SimpleObject, Clone, Debug, PartialEq, Eq)]
pub struct AlternativeDependency {
    pub(crate) options: Vec<AppMetadata>,
}

#[derive(Clone, Debug, PartialEq, Eq, Union)]
#[graphql(name = "Dependency")]
pub enum OutputDependency {
    OneDependency(AppMetadata),
    AlternativeDependency(AlternativeDependency),
}

#[derive(Deserialize, Clone, Debug, PartialEq, Eq)]
#[serde(untagged)]
pub enum Dependency {
    OneDependency(String),
    AlternativeDependency(Vec<String>),
}

#[derive(SimpleObject, Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub struct SettingsDeclaration {
    #[serde(rename = "type")]
    pub setting_type: SettingType,
    #[serde(default = "Vec::default")]
    #[serde(skip_serializing_if = "Vec::<String>::is_empty")]
    pub values: Vec<String>,
    pub name: BTreeMap<String, String>,
    pub description: BTreeMap<String, String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub default: Option<String>,
}

#[derive(SimpleObject, Serialize, Deserialize, Clone, Default, Debug, PartialEq, Eq)]
#[graphql(name = "VolumeDefinition")]
pub struct Volume {
    pub minimum_size: u64,
    pub recommended_size: u64,
    pub name: MultiLangItem,
    pub description: MultiLangItem,
}

#[derive(SimpleObject, Deserialize, Clone, Default, Debug, PartialEq, Eq)]
#[serde(rename_all = "camelCase")]
#[graphql(complex)]
pub struct AppMetadata {
    /// The app id
    pub id: String,
    /// The name of the app
    pub name: String,
    /// The version of the app
    pub version: String,
    /// The category for the app
    pub category: MultiLangItem,
    /// A short tagline for the app
    pub tagline: MultiLangItem,
    // Developer name -> their website
    pub developers: Json<BTreeMap<String, String>>,
    /// A description of the app
    pub description: MultiLangItem,
    #[serde(default)]
    #[graphql(skip)]
    /// Dependencies the app requires
    pub dependencies: Vec<Dependency>,
    /// Permissions the app has
    /// If a permission is from an app that is not listed in the dependencies, it is considered optional
    pub has_permissions: Vec<String>,
    /// App repository name -> repo URL
    pub repos: BTreeMap<String, String>,
    /// A support link for the app
    pub support: String,
    /// A list of promo images for the apps
    #[serde(default)]
    pub gallery: Vec<String>,
    /// The URL to the app icon
    pub icon: Option<String>,
    /// The path the "Open" link on the dashboard should lead to
    pub path: Option<String>,
    /// The app's default username
    pub default_username: Option<String>,
    /// The app's default password.
    pub default_password: Option<String>,
    /// For "virtual" apps, the service the app implements
    pub implements: Option<String>,
    #[serde(
        default,
        skip_serializing_if = "BTreeMap::<String, BTreeMap<String, String>>::is_empty"
    )]
    pub release_notes: BTreeMap<String, BTreeMap<String, String>>,
    /// The SPDX identifier of the app license
    pub license: String,
    /// Available settings for this app, directly copied from settings.json
    #[serde(
        default,
        skip_serializing_if = "BTreeMap::<String, SettingsDeclaration>::is_empty"
    )]
    pub settings: BTreeMap<String, SettingsDeclaration>,
    /// Whether this app should only allow one https domain
    pub single_https_domain: bool,
    /// Whether this app allows the user to change the domain
    pub allow_domain_change: bool,
    /// Volumes this app exposes
    pub volumes: HashMap<String, Volume>,
    /// Ports this app uses
    /// Before installing, app-manager clients need to check if any of these ports are already in use by other apps
    /// If so, the user needs to be notified
    pub ports: Vec<u16>,
    #[graphql(skip)]
    #[serde(skip)]
    pub(crate) owner: String,
    #[serde(skip_deserializing)]
    pub installed: bool,
    #[serde(skip)]
    #[graphql(skip)]
    pub latest_version: OnceLock<Option<AppUpdate>>,
    #[serde(skip_deserializing)]
    #[graphql(name = "dependencies")]
    pub dependencies_resolved: Vec<OutputDependency>,
}
