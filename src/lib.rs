#[cfg(not(feature = "__prisma_cli"))]
pub(crate) mod api;
#[cfg(not(feature = "__prisma_cli"))]
pub(crate) mod apps;
#[cfg(not(feature = "__prisma_cli"))]
pub mod graphql;
#[cfg(not(feature = "__prisma_cli"))]
pub mod instance;
#[cfg(not(feature = "__prisma_cli"))]
pub mod longhorn;
#[allow(unused, warnings)]
#[cfg(not(feature = "__prisma_cli"))]
pub mod prisma;
#[cfg(not(feature = "__prisma_cli"))]
pub mod utils;
