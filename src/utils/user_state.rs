use async_graphql::Context;

use crate::api::apps::UserState;
use crate::graphql::auth_ctx::UserCtx;
use crate::prisma::app_installation;

pub async fn get_user_state(ctx: &Context<'_>) -> Option<UserState> {
    let user = ctx.data_unchecked::<UserCtx>();
    let db_client = ctx.data_unchecked::<crate::prisma::PrismaClient>();
    let app_installations = db_client
        .app_installation()
        .find_many(vec![app_installation::owner_name::equals(
            user.username.clone(),
        )])
        .select(app_installation::select!({
            app_id
            settings
        }))
        .exec()
        .await
        .ok()?;
    let settings = app_installations
        .into_iter()
        // Serializing can basically never fail, because settings has a JSON type in the DB and is a serde_json::Value
        .map(|app| {
            (
                app.app_id,
                serde_json::to_string(&app.settings).expect("Failed to serialize app settings"),
            )
        })
        .collect::<std::collections::HashMap<_, _>>();

    Some(UserState {
        user: user.username.clone(),
        installed_apps: settings.keys().cloned().collect(),
        app_settings: settings,
    })
}
