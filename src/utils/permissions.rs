use serde::{Deserialize, Serialize};

use crate::prisma::Permission;

#[derive(Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct PermissionSet(u16);

impl From<Permission> for u16 {
    fn from(value: Permission) -> Self {
        match value {
            Permission::ManageApps => 0b0000000000000001,
            Permission::ManageDomains => 0b0000000000000010,
            Permission::ManageUsers => 0b0000000000000100,
            Permission::ManageGroups => 0b0000000000001000,
            Permission::ManageProxies => 0b0000000000010000,
            Permission::ManageIssuers => 0b0000000000100000,
            Permission::ConfigureNetwork => 0b0000000001000000,
            Permission::ManageSysComponents => 0b0000000010000000,
            Permission::ManageHardware => 0b0000000100000000,
            Permission::Escalate => 0b0000001000000000,
            Permission::UseNirvatiMe => 0b0000010000000000,
            Permission::ManageAppStores => 0b0000100000000000,
            Permission::ManageAppStorage => 0b0001000000000000,
        }
    }
}
impl PermissionSet {
    pub fn add(&mut self, perm: Permission) {
        self.0 |= <Permission as Into<u16>>::into(perm);
    }

    pub fn remove(&mut self, perm: Permission) {
        self.0 &= !<Permission as Into<u16>>::into(perm);
    }

    pub fn has(&self, perm: Permission) -> bool {
        self.0 & <Permission as Into<u16>>::into(perm) != 0
    }
}

impl From<&[Permission]> for PermissionSet {
    fn from(permissions: &[Permission]) -> Self {
        let mut set = PermissionSet(0);
        for perm in permissions {
            set.add(*perm);
        }
        set
    }
}

#[cfg(test)]
mod test {
    use crate::prisma::Permission;

    use super::PermissionSet;

    #[test]
    fn test_permission_set() {
        let mut set = PermissionSet(0);
        assert!(!set.has(Permission::ManageApps));
        set.add(Permission::ManageApps);
        assert!(set.has(Permission::ManageApps));
        set.add(Permission::ManageGroups);
        assert!(set.has(Permission::ManageApps));
        assert!(set.has(Permission::ManageGroups));
        set.remove(Permission::ManageApps);
        assert!(!set.has(Permission::ManageApps));
        assert!(set.has(Permission::ManageGroups));
    }
}
