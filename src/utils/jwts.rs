use serde::{Deserialize, Serialize};

use crate::utils::permissions::PermissionSet;

#[derive(Clone, Serialize, Deserialize)]
pub struct JwtAdditionalClaims {
    pub user_group: String,
    pub permission_set: PermissionSet,
    pub email: Option<String>,
}
