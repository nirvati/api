use std::sync::OnceLock;

use fancy_regex::Regex;

pub static EMAIL_REGEX: OnceLock<Regex> = OnceLock::new();

pub fn validate_email(email: &str) -> bool {
    // Does not allow quoted strings, comments, or unicode characters, and does not check for .. in the domain name
    // Early check to avoid regex
    if email.len() > 254 || !email.contains('@') || !email.contains('.') {
        return false;
    }
    let regex = EMAIL_REGEX.get_or_init(|| {
        Regex::new(r"^(?=[\w\-\.]+).{1,64}@([\w-]{1,63}\.)+[\w\-]{2,63}$").unwrap()
    });
    regex.is_match(email).unwrap_or(false)
}

#[cfg(test)]
mod tests {
    use crate::utils::email::validate_email;

    #[test]
    fn basic_email() {
        assert!(validate_email("info@nirvati.org"));
    }

    #[test]
    fn invalid_email() {
        assert!(!validate_email("info@nirvati"));
    }

    #[test]
    fn invalid_email_2() {
        assert!(!validate_email("info@nirvati."));
    }
}
