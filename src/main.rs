use std::str::FromStr;
use std::sync::OnceLock;

use actix_web::{
    guard, http::header::HeaderMap, web, App, HttpRequest, HttpResponse, HttpServer, Responder,
};
use async_graphql::{EmptySubscription, Schema};
use async_graphql_actix_web::{GraphQLRequest, GraphQLResponse};
use biscuit::jwa::SignatureAlgorithm;
use biscuit::jws::Secret;
use biscuit::{ClaimPresenceOptions, Presence, Validation, ValidationOptions, JWT};

use nirvati_api::graphql::auth_ctx::UserCtx;
use nirvati_api::graphql::{AppSchema, ChannelCtx, Mutation, Query};
use nirvati_api::instance::{InstanceType, INSTANCE_TYPE};
use nirvati_api::prisma::Permission;
use nirvati_api::utils::jwts::JwtAdditionalClaims;

static SECRET: OnceLock<Secret> = OnceLock::new();

fn get_user_ctx_from_headers(headers: &HeaderMap) -> Option<UserCtx> {
    let auth_header = headers.get("Authorization")?.to_str().ok()?;
    let auth_header = auth_header.split("Bearer ").collect::<Vec<_>>();
    if auth_header.len() != 2 {
        return None;
    }
    let token = auth_header[1];
    let jwt_secret = SECRET.get().expect("JWT secret not set");
    let token = JWT::<JwtAdditionalClaims, biscuit::Empty>::new_encoded(token);
    let options = ClaimPresenceOptions {
        expiry: Presence::Required,
        subject: Presence::Required,
        ..Default::default()
    };
    let token = token
        .into_decoded(jwt_secret, SignatureAlgorithm::HS256)
        .ok()?;
    token
        .validate(ValidationOptions {
            claim_presence_options: options,
            expiry: Validation::Validate(()),
            ..Default::default()
        })
        .ok()?;
    let (_, claims) = token.unwrap_decoded();
    Some(UserCtx {
        // Existence of subject is already validated
        username: claims.registered.subject.unwrap(),
        permissions: claims.private.permission_set,
        user_group: claims.private.user_group,
        email: claims.private.email,
    })
}

async fn options_main(_req: HttpRequest) -> impl Responder {
    HttpResponse::NoContent()
}

async fn graphql_main(
    schema: web::Data<AppSchema>,
    req: HttpRequest,
    gql_request: GraphQLRequest,
) -> GraphQLResponse {
    let mut request = gql_request.into_inner();
    if let Some(token) = get_user_ctx_from_headers(req.headers()) {
        request = request.data(token);
    }
    schema.execute(request).await.into()
}

/*async fn index_ws(
    schema: web::Data<AppSchema>,
    req: HttpRequest,
    payload: web::Payload,
) -> Result<HttpResponse> {
    GraphQLSubscription::new(Schema::clone(&*schema))
        .on_connection_init(on_connection_init)
        .start(&req, payload)
}*/

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    #[cfg(feature = "dotenvy")]
    dotenvy::dotenv().expect("Failed to load .env file");
    tracing_subscriber::fmt::init();
    let db_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    dbg!(&db_url);
    let bind_address = std::env::var("BIND_ADDRESS").expect("BIND_ADDRESS must be set");
    let core_endpoint = std::env::var("CORE_ENDPOINT").expect("CORE_ENDPOINT must be set");
    let apps_endpoint = std::env::var("APPS_ENDPOINT").expect("APPS_ENDPOINT must be set");
    let https_endpoint = std::env::var("HTTPS_ENDPOINT").expect("HTTPS_ENDPOINT must be set");
    let instance_type = std::env::var("INSTANCE_TYPE").expect("INSTANCE_TYPE must be set");
    let client = nirvati_api::prisma::new_client_with_url(&db_url)
        .await
        .expect("Failed to create database client");
    #[cfg(debug_assertions)]
    client
        ._db_push()
        .accept_data_loss()
        .await
        .expect("Failed to apply migrations!");
    #[cfg(not(debug_assertions))]
    client
        ._migrate_deploy()
        .await
        .expect("Failed to apply migrations!");
    let instance_type =
        InstanceType::from_str(&instance_type).expect("Failed to parse instance type");
    INSTANCE_TYPE
        .set(instance_type)
        .expect("Failed to set instance type");

    {
        let _ = client
            .user()
            .update(
                nirvati_api::prisma::user::UniqueWhereParam::UsernameEquals("admin".to_string()),
                vec![nirvati_api::prisma::user::SetParam::SetPermissions(vec![
                    Permission::ManageApps,
                    Permission::ManageDomains,
                    Permission::ManageUsers,
                    Permission::ManageGroups,
                    Permission::ManageProxies,
                    Permission::ManageIssuers,
                    Permission::ManageHardware,
                    Permission::ConfigureNetwork,
                    Permission::ManageSysComponents,
                    Permission::Escalate,
                    Permission::UseNirvatiMe,
                    Permission::ManageAppStorage,
                    Permission::ManageAppStores,
                ])],
            )
            .exec()
            .await;
    }
    let secret = std::env::var("JWT_SECRET").expect("JWT_SECRET must be set");
    let secret = Secret::Bytes(secret.into_bytes());

    let channel_ctx = {
        let core_endpoint = tonic::transport::Endpoint::from_shared(core_endpoint)
            .expect("Failed to create core endpoint");
        let apps_endpoint = tonic::transport::Endpoint::from_shared(apps_endpoint)
            .expect("Failed to create apps endpoint");
        let https_endpoint = tonic::transport::Endpoint::from_shared(https_endpoint)
            .expect("Failed to create https endpoint");
        let core_channel = core_endpoint
            .connect()
            .await
            .expect("Failed to connect to core");
        let apps_channel = apps_endpoint
            .connect()
            .await
            .expect("Failed to connect to apps");
        let https_channel = https_endpoint
            .connect()
            .await
            .expect("Failed to connect to https");
        ChannelCtx::new(core_channel, apps_channel, https_channel)
    };

    if SECRET.set(secret.clone()).is_err() {
        panic!("Failed to set JWT secret");
    }

    let kube_client = kube::Client::try_default()
        .await
        .expect("Failed to create kube client");
    let longhorn_client = nirvati_api::longhorn::Client::new(
        std::env::var("LONGHORN_ENDPOINT")
            .expect("LONGHORN_ENDPOINT must be set")
            .as_str(),
    );
    let schema = Schema::build(Query, Mutation::default(), EmptySubscription)
        .limit_recursive_depth(10)
        .data(channel_ctx)
        .data(client)
        .data(secret)
        .data(kube_client)
        .data(longhorn_client)
        .finish();

    HttpServer::new(move || {
        let app = App::new();
        #[cfg(feature = "__development")]
        let app = app.wrap(actix_cors::Cors::permissive());
        app.app_data(web::Data::new(schema.clone()))
            .service(
                web::resource("/v0/graphql")
                    .guard(guard::Post())
                    .to(graphql_main),
            )
            .service(
                web::resource("/v0/graphql")
                    .guard(guard::Options())
                    .to(options_main),
            )
    })
    .bind(&bind_address)?
    .run()
    .await
}
