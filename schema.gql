enum AcmeProvider {
	LETS_ENCRYPT
	BUY_PASS
}

type AlternativeDependency {
	options: [AppMetadata!]!
}

type AppDomain {
	name: String!
	createdAt: DateTime!
	appId: String!
	parentDomain: UserDomain
	user: User!
	ready: Boolean!
}

type AppDomainConnection {
	"""
	Information to aid in pagination.
	"""
	pageInfo: PageInfo!
	"""
	A list of edges.
	"""
	edges: [AppDomainEdge!]!
	"""
	A list of nodes.
	"""
	nodes: [AppDomain!]!
}

"""
An edge in a connection.
"""
type AppDomainEdge {
	"""
	The item at the end of the edge
	"""
	node: AppDomain!
	"""
	A cursor for use in pagination
	"""
	cursor: String!
}

type AppMetadata {
	"""
	The app id
	"""
	id: String!
	"""
	The name of the app
	"""
	name: String!
	"""
	The version of the app
	"""
	version: String!
	"""
	The category for the app
	"""
	category: MultiLangItem!
	"""
	A short tagline for the app
	"""
	tagline: MultiLangItem!
	developers: JSON!
	"""
	A description of the app
	"""
	description: MultiLangItem!
	"""
	Permissions the app has
	If a permission is from an app that is not listed in the dependencies, it is considered optional
	"""
	hasPermissions: [String!]!
	"""
	App repository name -> repo URL
	"""
	repos: JSONObject!
	"""
	A support link for the app
	"""
	support: String!
	"""
	A list of promo images for the apps
	"""
	gallery: [String!]!
	"""
	The URL to the app icon
	"""
	icon: String
	"""
	The path the "Open" link on the dashboard should lead to
	"""
	path: String
	"""
	The app's default username
	"""
	defaultUsername: String
	"""
	The app's default password.
	"""
	defaultPassword: String
	"""
	For "virtual" apps, the service the app implements
	"""
	implements: String
	releaseNotes: JSONObject!
	"""
	The SPDX identifier of the app license
	"""
	license: String!
	"""
	Available settings for this app, directly copied from settings.json
	"""
	settings: JSONObject!
	"""
	Whether this app should only allow one https domain
	"""
	singleHttpsDomain: Boolean!
	"""
	Whether this app allows the user to change the domain
	"""
	allowDomainChange: Boolean!
	"""
	Volumes this app exposes
	"""
	volumes: JSONObject!
	"""
	Ports this app uses
	Before installing, app-manager clients need to check if any of these ports are already in use by other apps
	If so, the user needs to be notified
	"""
	ports: [Int!]!
	installed: Boolean!
	dependencies: [Dependency!]!
	owner: User!
	latestVersion: AppUpdate
	domains: [AppDomain!]!
	runningVolumes: [Volume!]!
}

type AppStore {
	id: String!
	name: MultiLangItem!
	tagline: MultiLangItem!
	description: MultiLangItem!
	icon: String!
	developers: JSON!
	license: String!
	type: StoreType!
	src: String!
	apps: [AppMetadata!]!
	owner: User!
}

type AppUpdate {
	latestVersion: String!
	releasesNotes: JSON!
}


type CertManagerInfo {
	installedVersion: String!
	"""
	Returns the latest version of cert-manager that we can safely upgrade to (semver-wise).
	"""
	latestVersion: String!
}

"""
Implement the DateTime<Utc> scalar

The input/output is a string in RFC3339 format.
"""
scalar DateTime

union Dependency = AppMetadata | AlternativeDependency

enum DnsProvider {
	CLOUDFLARE
	NIRVATI_ME
	NONE
}



type Info {
	isUserSetup: Boolean!
	isSetupFinished: Boolean!
	instanceType: InstanceType!
}

enum InstanceType {
	LITE
	HOME
	ENTERPRISE
}


"""
A scalar that can represent any JSON value.
"""
scalar JSON

"""
A scalar that can represent any JSON Object value.
"""
scalar JSONObject

type K3Sinfo {
	installedVersion: String
	"""
	Returns the latest version of K3s that we can safely upgrade to, taking Kubernetes version skew into account.
	"""
	latestVersion: String
}

type LonghornInfo {
	installedVersion: String!
	"""
	Returns the latest version of Longhorn that we can safely upgrade to (semver-wise).
	"""
	latestVersion: String!
}

scalar MultiLangItem

type Mutation {
	moveNirvatiToChart: Boolean!
	createInitialUser(password: String!, name: String!, email: String): Boolean!
	finishSetup: Boolean!
	createUser(username: String!, password: String!, name: String!, email: String, permissions: [Permission!], group: String): Boolean!
	updateUser(username: String, password: String, name: String, email: String, eraseEmail: Boolean, permissions: [Permission!], group: String): Boolean!
	deleteUser(username: String!): Boolean!
	createUserGroup(name: String!): Boolean!
	login(username: String!, password: String!): String!
	upgradeK3S(targetVersion: String!): Boolean!
	upgradeCertManager(targetVersion: String!): Boolean!
	upgradeLonghorn(targetVersion: String!): Boolean!
	startTailscaleLogin: Boolean!
	startTailscale: Boolean!
	installApp(appId: String!, settings: JSON!, initialDomain: String!, acmeProvider: AcmeProvider!): Boolean!
	uninstallApp(appId: String!): Boolean!
	generate: Boolean!
	addAppStore(src: String!, storeType: StoreType!): Boolean!
	removeAppStore(src: String!): Boolean!
	updateApp(app: String!): Boolean!
	createAcmeIssuer(provider: AcmeProvider!, shareEmail: Boolean): Boolean!
	deleteAcmeIssuer(provider: AcmeProvider!): Boolean!
	addAppDomain(app: String!, domain: String!, acmeProvider: AcmeProvider!): Boolean!
	removeAppDomain(domain: String!): Boolean!
	addDomain(domain: String!, dnsProvider: DnsProvider, dnsAuth: String, acmeProviders: [AcmeProvider!]!): Boolean!
	removeDomain(domain: String!): Boolean!
	setReplicaCount(appId: String!, volumeId: String!, replicas: Int!): Boolean!
	resizeVolume(appId: String!, volumeId: String!, size: Int!): Boolean!
	createService(ip: String, dnsName: String, tcpPorts: [Int!]!, udpPorts: [Int!]!): String!
	addServicePorts(id: String!, tcpPorts: [Int!]!, udpPorts: [Int!]!): Boolean!
	deleteService(id: String!): Boolean!
	createRoute(proxyDomain: String!, localPath: String!, targetPath: String!, protocol: Protocol!, targetPort: Int!, enableCompression: Boolean!, serviceId: String!, allowInvalidCert: Boolean!, targetSni: String): String!
	deleteRoute(routeId: String!): Boolean!
	createProxy(domain: String!, acmeProvider: AcmeProvider!): Boolean!
	deleteProxy(domain: String!): Boolean!
}

type Network {
	ip: String!
	tailscaleInfo: TailscaleInfo!
}

type NirvatiMeDetails {
	createdAt: String!
	ip: String!
	lastPing: String!
	name: String!
}

"""
Information about pagination in a connection
"""
type PageInfo {
	"""
	When paginating backwards, are there more items?
	"""
	hasPreviousPage: Boolean!
	"""
	When paginating forwards, are there more items?
	"""
	hasNextPage: Boolean!
	"""
	When paginating backwards, the cursor to continue.
	"""
	startCursor: String
	"""
	When paginating forwards, the cursor to continue.
	"""
	endCursor: String
}

enum Permission {
	MANAGE_APPS
	MANAGE_DOMAINS
	MANAGE_USERS
	MANAGE_GROUPS
	MANAGE_PROXIES
	MANAGE_ISSUERS
	CONFIGURE_NETWORK
	MANAGE_SYS_COMPONENTS
	MANAGE_HARDWARE
	ESCALATE
	USE_NIRVATI_ME
	MANAGE_APP_STORES
	MANAGE_APP_STORAGE
}

enum Protocol {
	HTTP
	HTTPS
}

type Proxy {
	domain: String!
	owner: User!
	routes: [Route!]!
}

type Query {
	network: Network!
	info: Info!
	upgrades: Upgrades!
	users: Users!
}

type Route {
	id: String!
	enableCompression: Boolean!
	localPath: String!
	targetPath: String!
	protocol: Protocol!
	targetPort: Int!
	service: Service!
	owner: User!
}

type Service {
	id: String!
	upstream: String!
	upstreamType: UpstreamType!
	tcpPorts: [Int!]!
	udpPorts: [Int!]!
	owner: User!
}

enum StoreType {
	NIRVATI
	PLUGIN
}


type TailscaleInfo {
	ip: String
	authUrl: String
}

type Upgrades {
	k3S: K3Sinfo!
	certManager: CertManagerInfo!
	longhorn: LonghornInfo!
}

enum UpstreamType {
	DNS
	IP
}

type User {
	username: String!
	domains: [UserDomain!]!
	appDomains(after: String, before: String, first: Int, last: Int): AppDomainConnection!
	name: String!
	permissions: [Permission!]!
	email: String
	lastVerifiedEmail: String
	createdAt: DateTime!
	enabled: Boolean!
	acmeProviders: [AcmeProvider!]!
	stores: [AppStore!]!
	store(id: String!): AppStore!
	apps(installedOnly: Boolean): [AppMetadata!]!
	app(appId: String!): AppMetadata!
	jwt: String!
	entropy(identifier: String!): String!
	nirvatiMe: NirvatiMeDetails
	proxies: [Proxy!]!
	proxy(domain: String!): Proxy!
	services: [Service!]!
	service(id: String!): Service!
}

type UserConnection {
	"""
	Information to aid in pagination.
	"""
	pageInfo: PageInfo!
	"""
	A list of edges.
	"""
	edges: [UserEdge!]!
	"""
	A list of nodes.
	"""
	nodes: [User!]!
}

type UserDomain {
	createdAt: DateTime!
	domain: String!
	dnsProvider: DnsProvider!
	dnsProviderAuth: String
	user: User!
	appDomains(after: String, before: String, first: Int, last: Int): AppDomainConnection!
}

"""
An edge in a connection.
"""
type UserEdge {
	"""
	The item at the end of the edge
	"""
	node: User!
	"""
	A cursor for use in pagination
	"""
	cursor: String!
}

type Users {
	me: User!
	user(username: String!): User!
	users(searchNameId: String, userGroup: String, email: String, after: String, before: String, first: Int, last: Int): UserConnection!
}

type Volume {
	id: String!
	size: Int!
	replicaCount: Int!
	actualSize: Int!
	humanReadableId: String!
	volumeDefinition: VolumeDefinition!
}

type VolumeDefinition {
	minimumSize: Int!
	recommendedSize: Int!
	name: MultiLangItem!
	description: MultiLangItem!
}

directive @include(if: Boolean!) on FIELD | FRAGMENT_SPREAD | INLINE_FRAGMENT
directive @skip(if: Boolean!) on FIELD | FRAGMENT_SPREAD | INLINE_FRAGMENT
schema {
	query: Query
	mutation: Mutation
}
