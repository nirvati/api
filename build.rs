pub fn main() {
    tonic_build::configure()
        .protoc_arg("--experimental_allow_proto3_optional")
        .compile(
            &[
                "protos/core.proto",
                "protos/app-manager.proto",
                "protos/https.proto",
            ],
            &["protos"],
        )
        .unwrap();
}
