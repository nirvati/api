/*
  Warnings:

  - Added the required column `allowInvalidCert` to the `Route` table without a default value. This is not possible if the table is not empty.

*/
-- AlterEnum
-- This migration adds more than one value to an enum.
-- With PostgreSQL versions 11 and earlier, this is not possible
-- in a single migration. This can be worked around by creating
-- multiple migrations, each migration adding only one value to
-- the enum.


ALTER TYPE "Permission" ADD VALUE 'MANAGE_APP_STORES';
ALTER TYPE "Permission" ADD VALUE 'MANAGE_APP_STORAGE';

-- AlterTable
ALTER TABLE "Route" ADD COLUMN     "allowInvalidCert" BOOLEAN NOT NULL,
ADD COLUMN     "targetSni" TEXT;
