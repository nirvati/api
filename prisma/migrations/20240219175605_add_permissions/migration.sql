/*
  Warnings:

  - You are about to drop the column `role` on the `User` table. All the data in the column will be lost.

*/
-- CreateEnum
CREATE TYPE "Permission" AS ENUM ('MANAGE_APPS', 'MANAGE_DOMAINS', 'MANAGE_USERS', 'MANAGE_GROUPS', 'CREATE_PROXIES', 'MANAGE_ISSUERS', 'CONFIGURE_NETWORK', 'MANAGE_SYS_COMPONENTS', 'MANAGE_HARDWARE', 'ESCALATE');

-- AlterTable
ALTER TABLE "User" DROP COLUMN "role",
ADD COLUMN     "permissions" "Permission"[] DEFAULT ARRAY[]::"Permission"[];

-- DropEnum
DROP TYPE "Role";
