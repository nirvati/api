/*
  Warnings:

  - You are about to drop the column `httpsConsent` on the `User` table. All the data in the column will be lost.
  - You are about to drop the column `httpsShareEmail` on the `User` table. All the data in the column will be lost.

*/
-- CreateEnum
CREATE TYPE "AcmeProvider" AS ENUM ('LETS_ENCRYPT', 'BUY_PASS');

-- AlterTable
ALTER TABLE "AppDomain" ADD COLUMN     "acmeAccountId" TEXT;

-- AlterTable
ALTER TABLE "User" DROP COLUMN "httpsConsent",
DROP COLUMN "httpsShareEmail";

-- CreateTable
CREATE TABLE "AcmeAccount" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "email" TEXT,
    "provider" "AcmeProvider" NOT NULL,
    "ownerId" TEXT NOT NULL,

    CONSTRAINT "AcmeAccount_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "_AcmeAccountToDomain" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "AcmeAccount_id_key" ON "AcmeAccount"("id");

-- CreateIndex
CREATE UNIQUE INDEX "AcmeAccount_provider_ownerId_key" ON "AcmeAccount"("provider", "ownerId");

-- CreateIndex
CREATE UNIQUE INDEX "_AcmeAccountToDomain_AB_unique" ON "_AcmeAccountToDomain"("A", "B");

-- CreateIndex
CREATE INDEX "_AcmeAccountToDomain_B_index" ON "_AcmeAccountToDomain"("B");

-- AddForeignKey
ALTER TABLE "AcmeAccount" ADD CONSTRAINT "AcmeAccount_ownerId_fkey" FOREIGN KEY ("ownerId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "AppDomain" ADD CONSTRAINT "AppDomain_acmeAccountId_fkey" FOREIGN KEY ("acmeAccountId") REFERENCES "AcmeAccount"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_AcmeAccountToDomain" ADD CONSTRAINT "_AcmeAccountToDomain_A_fkey" FOREIGN KEY ("A") REFERENCES "AcmeAccount"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_AcmeAccountToDomain" ADD CONSTRAINT "_AcmeAccountToDomain_B_fkey" FOREIGN KEY ("B") REFERENCES "Domain"("id") ON DELETE CASCADE ON UPDATE CASCADE;
