/*
  Warnings:

  - You are about to drop the `App` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "App" DROP CONSTRAINT "App_installedById_fkey";

-- DropTable
DROP TABLE "App";
