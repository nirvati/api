-- AlterTable
ALTER TABLE "AppDomain" ADD COLUMN     "appInstallationId" TEXT;

-- CreateTable
CREATE TABLE "AppInstallation" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "ownerId" TEXT NOT NULL,
    "isGlobal" BOOLEAN NOT NULL DEFAULT false,
    "settings" JSONB NOT NULL,

    CONSTRAINT "AppInstallation_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "AppInstallation_id_key" ON "AppInstallation"("id");

-- CreateIndex
CREATE UNIQUE INDEX "AppInstallation_name_ownerId_key" ON "AppInstallation"("name", "ownerId");

-- AddForeignKey
ALTER TABLE "AppDomain" ADD CONSTRAINT "AppDomain_appInstallationId_fkey" FOREIGN KEY ("appInstallationId") REFERENCES "AppInstallation"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "AppInstallation" ADD CONSTRAINT "AppInstallation_ownerId_fkey" FOREIGN KEY ("ownerId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
