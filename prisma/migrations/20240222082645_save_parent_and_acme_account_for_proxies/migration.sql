-- AlterTable
ALTER TABLE "Proxy" ADD COLUMN     "acmeAccountId" TEXT,
ADD COLUMN     "parentDomainName" TEXT;

-- AddForeignKey
ALTER TABLE "Proxy" ADD CONSTRAINT "Proxy_acmeAccountId_fkey" FOREIGN KEY ("acmeAccountId") REFERENCES "AcmeAccount"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Proxy" ADD CONSTRAINT "Proxy_parentDomainName_fkey" FOREIGN KEY ("parentDomainName") REFERENCES "Domain"("id") ON DELETE SET NULL ON UPDATE CASCADE;
