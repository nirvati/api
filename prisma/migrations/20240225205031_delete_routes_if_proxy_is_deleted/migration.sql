-- DropForeignKey
ALTER TABLE "Route" DROP CONSTRAINT "Route_proxyDomain_fkey";

-- AddForeignKey
ALTER TABLE "Route" ADD CONSTRAINT "Route_proxyDomain_fkey" FOREIGN KEY ("proxyDomain") REFERENCES "Proxy"("domain") ON DELETE CASCADE ON UPDATE CASCADE;
