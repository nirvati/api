/*
  Warnings:

  - A unique constraint covering the columns `[name,ownerId]` on the table `AppDomain` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[name,ownerId]` on the table `Domain` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "AppDomain_name_ownerId_key" ON "AppDomain"("name", "ownerId");

-- CreateIndex
CREATE UNIQUE INDEX "Domain_name_ownerId_key" ON "Domain"("name", "ownerId");
