/*
  Warnings:

  - You are about to drop the column `ports` on the `Service` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Service" DROP COLUMN "ports",
ADD COLUMN     "tcpPorts" INTEGER[],
ADD COLUMN     "udpPorts" INTEGER[];
