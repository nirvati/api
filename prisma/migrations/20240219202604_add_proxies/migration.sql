/*
  Warnings:

  - The values [CREATE_PROXIES] on the enum `Permission` will be removed. If these variants are still used in the database, this will fail.

*/
-- CreateEnum
CREATE TYPE "Protocol" AS ENUM ('HTTP', 'HTTPS');

-- AlterEnum
BEGIN;
CREATE TYPE "Permission_new" AS ENUM ('MANAGE_APPS', 'MANAGE_DOMAINS', 'MANAGE_USERS', 'MANAGE_GROUPS', 'MANAGE_PROXIES', 'MANAGE_ISSUERS', 'CONFIGURE_NETWORK', 'MANAGE_SYS_COMPONENTS', 'MANAGE_HARDWARE', 'ESCALATE');
ALTER TABLE "User" ALTER COLUMN "permissions" DROP DEFAULT;
ALTER TABLE "User" ALTER COLUMN "permissions" TYPE "Permission_new"[] USING ("permissions"::text::"Permission_new"[]);
ALTER TYPE "Permission" RENAME TO "Permission_old";
ALTER TYPE "Permission_new" RENAME TO "Permission";
DROP TYPE "Permission_old";
ALTER TABLE "User" ALTER COLUMN "permissions" SET DEFAULT ARRAY[]::"Permission"[];
COMMIT;

-- CreateTable
CREATE TABLE "Proxy" (
    "domain" TEXT NOT NULL,
    "ownerName" TEXT NOT NULL,

    CONSTRAINT "Proxy_pkey" PRIMARY KEY ("domain")
);

-- CreateTable
CREATE TABLE "Route" (
    "id" TEXT NOT NULL,
    "proxyDomain" TEXT NOT NULL,
    "enableCompression" BOOLEAN NOT NULL,
    "localPrefix" TEXT NOT NULL,
    "serviceId" TEXT NOT NULL,
    "targetProtocol" "Protocol" NOT NULL,

    CONSTRAINT "Route_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Service" (
    "id" TEXT NOT NULL,
    "dnsName" TEXT NOT NULL,
    "ports" TEXT[],
    "ownerName" TEXT NOT NULL,

    CONSTRAINT "Service_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Proxy_domain_key" ON "Proxy"("domain");

-- CreateIndex
CREATE UNIQUE INDEX "Route_id_key" ON "Route"("id");

-- CreateIndex
CREATE UNIQUE INDEX "Service_id_key" ON "Service"("id");

-- AddForeignKey
ALTER TABLE "Proxy" ADD CONSTRAINT "Proxy_ownerName_fkey" FOREIGN KEY ("ownerName") REFERENCES "User"("username") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Route" ADD CONSTRAINT "Route_proxyDomain_fkey" FOREIGN KEY ("proxyDomain") REFERENCES "Proxy"("domain") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Route" ADD CONSTRAINT "Route_serviceId_fkey" FOREIGN KEY ("serviceId") REFERENCES "Service"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Service" ADD CONSTRAINT "Service_ownerName_fkey" FOREIGN KEY ("ownerName") REFERENCES "User"("username") ON DELETE RESTRICT ON UPDATE CASCADE;
