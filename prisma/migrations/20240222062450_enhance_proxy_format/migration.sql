/*
  Warnings:

  - You are about to drop the column `localPrefix` on the `Route` table. All the data in the column will be lost.
  - You are about to drop the column `dnsName` on the `Service` table. All the data in the column will be lost.
  - The `ports` column on the `Service` table would be dropped and recreated. This will lead to data loss if there is data in the column.
  - A unique constraint covering the columns `[upstream,ownerName]` on the table `Service` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `localPath` to the `Route` table without a default value. This is not possible if the table is not empty.
  - Added the required column `targetPath` to the `Route` table without a default value. This is not possible if the table is not empty.
  - Added the required column `targetPort` to the `Route` table without a default value. This is not possible if the table is not empty.
  - Added the required column `type` to the `Service` table without a default value. This is not possible if the table is not empty.
  - Added the required column `upstream` to the `Service` table without a default value. This is not possible if the table is not empty.

*/
-- CreateEnum
CREATE TYPE "UpstreamType" AS ENUM ('IP', 'DNS');

-- AlterTable
ALTER TABLE "Route" DROP COLUMN "localPrefix",
ADD COLUMN     "localPath" TEXT NOT NULL,
ADD COLUMN     "targetPath" TEXT NOT NULL,
ADD COLUMN     "targetPort" INTEGER NOT NULL;

-- AlterTable
ALTER TABLE "Service" DROP COLUMN "dnsName",
ADD COLUMN     "type" "UpstreamType" NOT NULL,
ADD COLUMN     "upstream" TEXT NOT NULL,
DROP COLUMN "ports",
ADD COLUMN     "ports" INTEGER[];

-- CreateTable
CREATE TABLE "TcpProxy" (
    "domain" TEXT NOT NULL,
    "ownerName" TEXT NOT NULL,
    "serviceId" TEXT NOT NULL,

    CONSTRAINT "TcpProxy_pkey" PRIMARY KEY ("domain")
);

-- CreateIndex
CREATE UNIQUE INDEX "TcpProxy_domain_key" ON "TcpProxy"("domain");

-- CreateIndex
CREATE UNIQUE INDEX "Service_upstream_ownerName_key" ON "Service"("upstream", "ownerName");

-- AddForeignKey
ALTER TABLE "TcpProxy" ADD CONSTRAINT "TcpProxy_ownerName_fkey" FOREIGN KEY ("ownerName") REFERENCES "User"("username") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TcpProxy" ADD CONSTRAINT "TcpProxy_serviceId_fkey" FOREIGN KEY ("serviceId") REFERENCES "Service"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
