/*
  Warnings:

  - Made the column `acmeAccountId` on table `AppDomain` required. This step will fail if there are existing NULL values in that column.

*/
-- DropForeignKey
ALTER TABLE "AppDomain" DROP CONSTRAINT "AppDomain_acmeAccountId_fkey";

-- AlterTable
ALTER TABLE "AppDomain" ALTER COLUMN "acmeAccountId" SET NOT NULL;

-- AddForeignKey
ALTER TABLE "AppDomain" ADD CONSTRAINT "AppDomain_acmeAccountId_fkey" FOREIGN KEY ("acmeAccountId") REFERENCES "AcmeAccount"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
