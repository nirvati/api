-- CreateTable
CREATE TABLE "AppDomain" (
    "name" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "ownerId" TEXT NOT NULL,
    "appId" TEXT NOT NULL,
    "parentDomainName" TEXT,

    CONSTRAINT "AppDomain_pkey" PRIMARY KEY ("name")
);

-- CreateIndex
CREATE UNIQUE INDEX "AppDomain_name_key" ON "AppDomain"("name");

-- AddForeignKey
ALTER TABLE "AppDomain" ADD CONSTRAINT "AppDomain_ownerId_fkey" FOREIGN KEY ("ownerId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "AppDomain" ADD CONSTRAINT "AppDomain_parentDomainName_fkey" FOREIGN KEY ("parentDomainName") REFERENCES "Domain"("name") ON DELETE SET NULL ON UPDATE CASCADE;
