-- DropForeignKey
ALTER TABLE "AppDomain" DROP CONSTRAINT "AppDomain_appInstallationId_fkey";

-- AddForeignKey
ALTER TABLE "AppDomain" ADD CONSTRAINT "AppDomain_appInstallationId_fkey" FOREIGN KEY ("appInstallationId") REFERENCES "AppInstallation"("id") ON DELETE CASCADE ON UPDATE CASCADE;
